package com.tech4planet.cropscrap.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.tech4planet.cropscrap.Fragment.DutchIt_Debt_Fragment;
import com.tech4planet.cropscrap.Fragment.DutchIt_SharedBill_Fragment;
import com.tech4planet.cropscrap.Fragment.DutchIt_Transaction_Fragment;
import com.tech4planet.cropscrap.R;

public class DutchItActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    Button b_debt, b_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dutchit_sharedbill_fragment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPagerDutchIt);
        viewPager.setAdapter(new pagerAdapter(getSupportFragmentManager()));
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.dutchit2x);
        tabLayout.getTabAt(1).setIcon(R.drawable.dutchit2x);
        tabLayout.getTabAt(2).setIcon(R.drawable.dutchit2x);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    DutchIt_SharedBill_Fragment dutchIt_sharedBill_fragment = new DutchIt_SharedBill_Fragment();
                    return dutchIt_sharedBill_fragment;
                case 1:
                    DutchIt_Debt_Fragment dutchIt_debt_fragment = new DutchIt_Debt_Fragment();
                    return dutchIt_debt_fragment;
                case 2:
                    DutchIt_Transaction_Fragment dutchIt_transaction_fragment = new DutchIt_Transaction_Fragment();
                    return dutchIt_transaction_fragment;
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Shared-Bills";
                case 1:
                    return "Debt-Payment";
                case 2:
                    return "Transactions";
                default:
                    return null;
            }
        }
    }


}
