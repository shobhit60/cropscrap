package com.tech4planet.cropscrap.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.astuetz.PagerSlidingTabStrip;
import com.tech4planet.cropscrap.Fragment.IssueBillFragment;
import com.tech4planet.cropscrap.Fragment.ReceiveBillFragment;
import com.tech4planet.cropscrap.Fragment.ViewIssueBillFragment;
import com.tech4planet.cropscrap.Fragment.ViewReceiveBillFragment;
import com.tech4planet.cropscrap.R;

public class EbillActivity extends AppCompatActivity {
    //This is our tablayout
    private PagerSlidingTabStrip tabsStrip;

    //This is our viewPager
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ebill_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), 4);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Initializing the tablayout
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabsStrip);
        //Adding the tabs using addTab() method
        tabsStrip.setViewPager(viewPager);


        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);

            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static class Pager extends FragmentStatePagerAdapter {

        //integer to count number of tabs
        int tabCount;

        //Constructor to the class
        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            //Initializing tab count
            this.tabCount = tabCount;
        }

        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    IssueBillFragment issueBillFragment = new IssueBillFragment();

                    return issueBillFragment;
                case 1:
                    ViewIssueBillFragment viewIssueBillFragment = new ViewIssueBillFragment();
                    return viewIssueBillFragment;

                case 2:
                    ReceiveBillFragment receiveBillFragment = new ReceiveBillFragment();
                    return receiveBillFragment;

                case 3:
                    ViewReceiveBillFragment viewReceiveBillFragment = new ViewReceiveBillFragment();
                    return viewReceiveBillFragment;
                default:
                    return null;
            }
        }

        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Issue Bills";
                case 1:
                    return "View Issue Bills";
                case 2:
                    return "Receive Bills";
                case 3:
                    return "View Receive Bills";
                default:
                    return null;
            }
        }

    }
}
