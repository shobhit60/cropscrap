package com.tech4planet.cropscrap.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tech4planet.cropscrap.Adapter.ContactAdapter;
import com.tech4planet.cropscrap.Fragment.GroupFragment;
import com.tech4planet.cropscrap.Fragment.GroupListFragment;
import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;


public class GroupActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener,/*SearchView.OnQueryTextListener,*/GroupFragment.group_creation {
    RelativeLayout ll_bottom_slideUP;
    LinearLayout ll_bottomSlider;
    FrameLayout fl_screenblocker;
    boolean isBottomDrawerOpen = false;
    ArrayList<ContactModel> temp;
    RecyclerView slider_recyclerview;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    EditText searcheditext;
    FloatingActionButton fab;
    //This is our tablayout
    private TabLayout tabLayout;
    //This is our viewPager
    private ViewPager viewPager;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                for (ContactModel contactModel : MainScreen.list) {
                    if (contactModel.isSelected())
                        contactModel.setSelected(false);

                }
            }
        }).start();
        slider_recyclerview = (RecyclerView) findViewById(R.id.slider_recyclerview);
        layoutManager = new LinearLayoutManager(this);
        slider_recyclerview.setLayoutManager(layoutManager);
        slider_recyclerview.setHasFixedSize(true);
        adapter = new ContactAdapter(this, MainScreen.list);
        slider_recyclerview.setAdapter(adapter);
        ll_bottomSlider = (LinearLayout) findViewById(R.id.ll_bottomSlider);
        ll_bottom_slideUP = (RelativeLayout) findViewById(R.id.ll_bottom_slideUP);
        fl_screenblocker = (FrameLayout) findViewById(R.id.fl_screenBlocker);
        fl_screenblocker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBottomSlider();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.pager);
        //Creating our pager adapter
        final Pager adapter1 = new Pager(getSupportFragmentManager(), 2);
        //Adding adapter to pager
        viewPager.setAdapter(adapter1);
        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        //Adding the tabs using addTab() method
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.icgroup2x);
        tabLayout.getTabAt(1).setIcon(R.drawable.icgroup2x);


        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);


        searcheditext = (EditText) findViewById(R.id.searcheditext);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBottomSlider();
                adapter = new ContactAdapter(GroupActivity.this, MainScreen.list);
                slider_recyclerview.setAdapter(adapter);
                final Pager adapter1 = new Pager(getSupportFragmentManager(), 2);
                //Adding adapter to pager
                viewPager.setAdapter(adapter1);
                //Initializing the tablayout
                // tabLayout = (TabLayout) findViewById(R.id.tabLayout);
                //Adding the tabs using addTab() method
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.getTabAt(0).setIcon(R.drawable.icgroup2x);
                tabLayout.getTabAt(1).setIcon(R.drawable.icgroup2x);
              /*  startActivity(new Intent(GroupActivity.this,GroupActivity.class));
                finish();*/
            }
        });

        addTextListener();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void group_create(int id) {


        toggleBottomSlider();
    }

    public void toggleBottomSlider() {
        if (isBottomDrawerOpen) {
            isBottomDrawerOpen = false;
            Animation bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down);
            ll_bottom_slideUP.startAnimation(bottomDown);
            ll_bottom_slideUP.setVisibility(View.GONE);
            Thread timerThread = new Thread() {
                public void run() {
                    try {
                        sleep(600);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ll_bottomSlider.setVisibility(View.GONE);
                            }
                        });

                    }
                }
            };
            timerThread.start();

        } else {
            ll_bottomSlider.setVisibility(View.VISIBLE);
            isBottomDrawerOpen = true;
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up);
            ll_bottom_slideUP.startAnimation(bottomUp);
            ll_bottom_slideUP.setVisibility(View.VISIBLE);
        }

    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText=newText.toLowerCase();
        temp=new ArrayList<ContactModel>();

        for(ContactModel model:MainScreen.list)
        {
            String name=model.getName().toLowerCase().toString();
            if(name.contains(newText)){
                temp.add(model);
            }
        }
        ContactAdapter.filter(temp);
        adapter.notifyDataSetChanged();
        return true;
    }*/

    public void addTextListener() {

        searcheditext.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                temp = new ArrayList<ContactModel>();
                for (ContactModel model : MainScreen.list) {
                    String name = model.getName().toLowerCase().toString();
                    if (name.contains(query)) {
                        temp.add(model);
                    }
                }

                slider_recyclerview.setLayoutManager(new LinearLayoutManager(GroupActivity.this));
                adapter = new ContactAdapter(GroupActivity.this, temp);
                slider_recyclerview.setAdapter(adapter);
                adapter.notifyDataSetChanged();  // data set changed
            }
        });
    }

    public static class Pager extends FragmentStatePagerAdapter {

        //integer to count number of tabs
        int tabCount;

        //Constructor to the class
        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            //Initializing tab count
            this.tabCount = tabCount;
        }

        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    GroupFragment groupFragment = new GroupFragment();

                    return groupFragment;
                case 1:
                    GroupListFragment groupListFragment = new GroupListFragment();
                    return groupListFragment;
                default:
                    return null;
            }
        }

        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Group";
                case 1:
                    return "GroupList";
                default:
                    return null;
            }
        }
    }
}
