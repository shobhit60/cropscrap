package com.tech4planet.cropscrap.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tech4planet.cropscrap.Background.GroupDetailApi;
import com.tech4planet.cropscrap.R;

public class GroupDetail extends AppCompatActivity {

    RecyclerView grp_detail_recyclerview;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        grp_detail_recyclerview=(RecyclerView)findViewById(R.id.grp_detail_recyclerview);
        layoutManager = new LinearLayoutManager(this);
        grp_detail_recyclerview.setLayoutManager(layoutManager);
        grp_detail_recyclerview.setNestedScrollingEnabled(false);
        grp_detail_recyclerview.setHasFixedSize(true);
        Bundle bundle=getIntent().getExtras();
        String grp_id=bundle.getString("grp_id");
        new GroupDetailApi(this,grp_id,"grp_list_fragment",grp_detail_recyclerview,adapter).execute("http://13.126.131.147/admin/group_details/");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
