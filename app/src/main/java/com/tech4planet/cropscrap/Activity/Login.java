package com.tech4planet.cropscrap.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.tech4planet.cropscrap.Background.FaceBookLoginApi;
import com.tech4planet.cropscrap.Background.RegistrationApi;
import com.tech4planet.cropscrap.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    public static final String PACKAGE = "com.tech4planet.cropscrap";
    private static final int RC_SIGN_IN = 100;
    private static final String TAG = "GoogleSignIn";
    public static ProgressBar progress_login;
    public static boolean googlechange = false;
    TextView tv_signup;
    public static Button b_signIn;
    EditText et_email, et_password;
    String userid, password;
    ImageView googlebutton;
    GoogleApiClient mGoogleApiClient;
    Login login;

    //facebook variable
    ImageView facebookbutton;
    CallbackManager mFacebookCallbackManager;
    String FBTAG = "FacebookLogin";
    LoginManager mLoginManager;
    AccessTokenTracker mAccessTokenTracker;

    //internet connection variable
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setContentView(R.layout.activity_login);
        googlebutton = (ImageView) findViewById(R.id.googlebutton);
        googlebutton.setOnClickListener(this);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        progress_login = (ProgressBar) findViewById(R.id.progress_login);
        login = this;
        tv_signup = (TextView) findViewById(R.id.tv_signup);
        tv_signup.setOnClickListener(this);
        b_signIn = (Button) findViewById(R.id.b_signIn);
        b_signIn.setOnClickListener(this);

        //initialization variable for internet connection
        connMgr=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        //signin details for facebook
        facebookbutton=(ImageView)findViewById(R.id.facebookbutton);
        setupFacebookStuff();
        facebookbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleFacebookLogin();
            }
        });

        //signin details for google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }


    }

    private void setupFacebookStuff() {

        // This should normally be on your application class
        FacebookSdk.sdkInitialize(getApplicationContext());

       /* mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                updateFacebookButtonUI();
            }
        };*/

        mLoginManager = LoginManager.getInstance();
        mFacebookCallbackManager = CallbackManager.Factory.create();


    }

    private void handleFacebookLogin() {
        if (AccessToken.getCurrentAccessToken() != null){
            mLoginManager.logOut();
        }else{
            //mAccessTokenTracker.startTracking();
            mLoginManager.logInWithReadPermissions(Login.this, Arrays.asList("public_profile","email"));
            LoginManager.getInstance().registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    //updateFacebookButtonUI();
                    getUserDetails(loginResult);

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            });
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            googlechange = true;
            GoogleSignInAccount acct = result.getSignInAccount();
            new RegistrationApi(login, acct.getDisplayName(), acct.getEmail(), "null", "null").execute("http://saurabhshukla.info/api/signup01");
            final SharedPreferences sharedPreferences = Login.this.getSharedPreferences("googleacc", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("googlelogin", true);
            editor.commit();
        } else {
            progress_login.setVisibility(View.GONE);
            b_signIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.googlebutton:
                b_signIn.setVisibility(View.INVISIBLE);
                progress_login.setVisibility(View.VISIBLE);
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.tv_signup:
                Intent intent = new Intent(Login.this, SignUp.class);
                startActivity(intent);
                break;
            case R.id.b_signIn:
                userid = et_email.getText().toString();
                password = et_password.getText().toString();
                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else if (!TextUtils.isEmpty(userid) && !TextUtils.isEmpty(password)) {
                    b_signIn.setVisibility(View.INVISIBLE);
                    progress_login.setVisibility(View.VISIBLE);
                    new Mylogin().execute("http://www.cropscrap.com/Home/login01");
                } else
                    Toast.makeText(Login.this, "Please Enter Both Email And Password", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void setUserProfile(JSONObject response) {

        try {

            new RegistrationApi(login, response.get("formattedName").toString(), response.get("emailAddress").toString(), "null", "null").execute("http://saurabhshukla.info/api/signup01");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        String id,name,email;

                        try {
                            id=json_object.getString("id");
                            name=json_object.getString("name");
                            email=json_object.getString("email");
                            Log.d("value of id",id);
                            Log.d("value of name",name);
                            Log.d("value of email",email);
                            new FaceBookLoginApi(Login.this,id,name,email).execute("http://www.cropscrap.com/Home/login1");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email, picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }
    //background thread for normal login
    public class Mylogin extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", userid)
                    .add("password", password)
                    .add("medium", "phone")
                    .build();
            Request request = new Request.Builder()
                    .url(params[0])
                    .post(formBody)
                    .build();
            try (Response response = client.newCall(request).execute()) {

                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("value of login", s);
            try {

                JSONObject jsonObject=new JSONObject(s);
                   // JSONArray jsonArray=new JSONArray(s);
                JSONObject datajsonObject = jsonObject.getJSONObject("data");
                String result=jsonObject.getString("status");
                if (result.equals("success")) {
                    SplashScreen.user_mem_id = datajsonObject.getString("member_id");
                    final SharedPreferences sharedPreferences = Login.this.getSharedPreferences("normallogin", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("login", true);
                    editor.putString("memberid_db", SplashScreen.user_mem_id);
                    editor.commit();
                    Intent intent = new Intent(Login.this, MainScreen.class);
                    startActivity(intent);
                    finish();
                } else if (result.equals("error")) {
                    b_signIn.setVisibility(View.VISIBLE);
                    progress_login.setVisibility(View.GONE);
                    Toast.makeText(Login.this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Log.d("exception",e.toString());
                e.printStackTrace();
            }

        }
    }
    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                dialog.cancel();
            }
        });

        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }
}
