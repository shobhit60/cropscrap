package com.tech4planet.cropscrap.Activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tech4planet.cropscrap.Background.ProfileApi;
import com.tech4planet.cropscrap.Fragment.Fragment2;
import com.tech4planet.cropscrap.Fragment.Fragment3;
import com.tech4planet.cropscrap.Fragment.VP_MainScreen_Fragment;
import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;
import java.util.HashSet;

public class MainScreen extends AppCompatActivity {
    public static ArrayList<ContactModel> list = new ArrayList<ContactModel>();
    public static String name,email,contact;
    ViewPager main_screen_viewPager;
    /*public static ArrayList<String> grouplist=new ArrayList<>();*/
    ContactModel contactModel;
    private LinearLayout ll_group_nav, ll_friend_nav,ll_setting_nav,ll_share_nav,ll_rate_nav,ll_feeback_nav;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        main_screen_viewPager = (ViewPager) findViewById(R.id.main_screen_viewPager);
        main_screen_viewPager.setAdapter(new pagerAdapter(getSupportFragmentManager()));

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerView = (LinearLayout) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);


        ll_group_nav = (LinearLayout) findViewById(R.id.ll_group_nav);
        View.OnClickListener group_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainScreen.this, GroupActivity.class));
            }
        };
        ll_group_nav.setOnClickListener(group_click_listener);

        ll_friend_nav = (LinearLayout) findViewById(R.id.ll_friend_nav);
        View.OnClickListener friend_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainScreen.this, FriendActivity.class));
            }
        };
        ll_friend_nav.setOnClickListener(friend_click_listener);

        ll_setting_nav=(LinearLayout)findViewById(R.id.ll_setting_nav);
        View.OnClickListener setting_click_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainScreen.this,SettingActivity.class));
            }
        };
        ll_setting_nav.setOnClickListener(setting_click_listener);

        ll_share_nav=(LinearLayout)findViewById(R.id.ll_share_nav);
        View.OnClickListener share_click_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainScreen.this,"Work in Progress",Toast.LENGTH_SHORT).show();
            }
        };
        ll_share_nav.setOnClickListener(share_click_listener);

        ll_rate_nav=(LinearLayout)findViewById(R.id.ll_rate_nav);
        View.OnClickListener rate_click_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainScreen.this,"Work in Progress",Toast.LENGTH_SHORT).show();
            }
        };
        ll_rate_nav.setOnClickListener(rate_click_listener);

        ll_feeback_nav=(LinearLayout)findViewById(R.id.ll_feeback_nav);
        View.OnClickListener feedback_click_listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainScreen.this,"Work in Progress",Toast.LENGTH_SHORT).show();
            }
        };
        ll_feeback_nav.setOnClickListener(feedback_click_listener);
        new Thread(new Runnable() {
            @Override
            public void run() {
                ContentResolver cr = MainScreen.this.getContentResolver();
                Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                if (cursor.moveToFirst()) {

                    HashSet<String> filterdata=new HashSet<String>();
                    Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                    while (phones.moveToNext()) {
                        String normalized_no=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        normalized_no=normalized_no.replaceAll("[- ]","");
                        if(filterdata.add(normalized_no))
                        {
                            contactModel = new ContactModel();
                            String username = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            String userphoneNumber = normalized_no;
                            contactModel.setName(username);
                            contactModel.setNumber(userphoneNumber);
                            contactModel.setSelected(false);
                            list.add(contactModel);
                            Log.w("username",username);
                            Log.w("phoneno",userphoneNumber);
                        }

                        }
                    phones.close();
                }
            }
        }).start();

        new ProfileApi(this).execute("http://13.126.131.147/profile/");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerView)) {
                    Log.d("Drawer opened", "yes");
                    mDrawerLayout.closeDrawer(mDrawerView);
                } else {
                    Log.d("Drawer opened", "no");
                    mDrawerLayout.openDrawer(mDrawerView);
                }
                break;

        }

        return true;
    }

    public class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    VP_MainScreen_Fragment addChequeFragment = new VP_MainScreen_Fragment();
                    return addChequeFragment;
                case 1:
                    Fragment2 viewChequeFragment = new Fragment2();
                    return viewChequeFragment;
                case 2:
                    Fragment3 fragment3 = new Fragment3();
                    return fragment3;
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

    }
}
