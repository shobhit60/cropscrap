package com.tech4planet.cropscrap.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.tech4planet.cropscrap.Fragment.AddReminderRemindMeFragment;
import com.tech4planet.cropscrap.Fragment.CalenderDialogFragment;
import com.tech4planet.cropscrap.Fragment.CalenderFragment;
import com.tech4planet.cropscrap.Fragment.TimerFragment;
import com.tech4planet.cropscrap.Fragment.ViewEventRemindMeFragment;
import com.tech4planet.cropscrap.R;

public class RemindMeActivity extends AppCompatActivity implements AddReminderRemindMeFragment.AddReminderCalender,CalenderFragment.SelectSDate /*TimerFragment.SetTimer*/ {
    ViewPager viewPager;
    TabLayout tabLayout;
    CalenderDialogFragment calenderDialogFragment;
    int calender_selected_text_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remind_me);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPagerRemindMe);
        viewPager.setAdapter(new pagerAdapter(getSupportFragmentManager()));
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_notifications_white_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_notifications_white_24dp);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void Add_Reminder_Calender(int id) {

        switch (id) {
            case R.id.fromcalender:
                calender_selected_text_id = id;
                break;
            case R.id.tocalender:
                calender_selected_text_id = id;
                break;
        }
        FragmentManager fm = getSupportFragmentManager();
        calenderDialogFragment = new CalenderDialogFragment();
        calenderDialogFragment.show(fm, "FragmentDialog");

    }

   /* @Override
    public void set_timer(int id, String timer) {
        if (calender_selected_text_id == R.id.fromcalender) {
            switch (id) {

                case R.id.ok_btn:
                    ((TextView) findViewById(R.id.fromcalender)).setText(timer);
                    calenderDialogFragment.dismiss();
                    break;
                case R.id.cancel_btn:
                    ((TextView) findViewById(R.id.fromcalender)).setText("From *");
                    calenderDialogFragment.dismiss();
                    break;
            }

        } else {
            switch (id) {

                case R.id.ok_btn:
                    ((TextView) findViewById(R.id.tocalender)).setText(timer);
                    calenderDialogFragment.dismiss();
                    break;
                case R.id.cancel_btn:
                    ((TextView) findViewById(R.id.tocalender)).setText("To *");
                    calenderDialogFragment.dismiss();
                    break;
            }
        }

    }*/

    @Override
    public void Select_Date(String month, String day, String year) {
        if (calender_selected_text_id == R.id.fromcalender){
            ((TextView) findViewById(R.id.fromcalender)).setText(month+"-"+day+"-"+year);
            calenderDialogFragment.dismiss();
        }
        else
        {
            ((TextView) findViewById(R.id.tocalender)).setText(month+"-"+day+"-"+year);
            calenderDialogFragment.dismiss();
        }

    }

    public class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    AddReminderRemindMeFragment fragmentRemindersRemindMe = new AddReminderRemindMeFragment();
                    return fragmentRemindersRemindMe;
                case 1:
                    ViewEventRemindMeFragment fragmentAppointmentsRemindMe = new ViewEventRemindMeFragment();
                    return fragmentAppointmentsRemindMe;
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Add Reminder";
                case 1:
                    return "View Event";
                default:
                    return null;
            }
        }
    }


}
