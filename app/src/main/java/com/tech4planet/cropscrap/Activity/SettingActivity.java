package com.tech4planet.cropscrap.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tech4planet.cropscrap.Fragment.AddChequeFragment;
import com.tech4planet.cropscrap.Fragment.UserProfileFragment;
import com.tech4planet.cropscrap.Fragment.VendorProfileFragment;
import com.tech4planet.cropscrap.Fragment.ViewChequeFragment;
import com.tech4planet.cropscrap.R;

public class SettingActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPagerDigiCheque);
        viewPager.setAdapter(new pagerAdapter(getSupportFragmentManager()));
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.icperson2x);
        tabLayout.getTabAt(1).setIcon(R.drawable.icperson2x);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    UserProfileFragment userProfileFragment = new UserProfileFragment();
                    return userProfileFragment;
                case 1:
                    VendorProfileFragment vendorProfileFragment = new VendorProfileFragment();
                    return vendorProfileFragment;
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "User Profile";
                case 1:
                    return "Vendor Profile";
                default:
                    return null;
            }
        }
    }
}
