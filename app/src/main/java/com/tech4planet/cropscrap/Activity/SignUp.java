package com.tech4planet.cropscrap.Activity;


import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tech4planet.cropscrap.Background.RegistrationApi;
import com.tech4planet.cropscrap.R;
import com.tech4planet.cropscrap.OtherFiles.okhttp;

import java.util.Random;

public class SignUp extends AppCompatActivity {
    public static ProgressBar progress_signup;
    public String otp = "";
    String username;
    String useremail;
    String password;
    String confirmed_password;
    Button b_signup;
    EditText et_name;
    EditText et_email;
    EditText et_password;
    EditText et_confirmed_password;
    EditText et_mobile;
    Button b_sendOtp;
    EditText et_otp;
    SignUp signUp;
    private String otp_recieived = "";
    private String usermobile;

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_name = (EditText) findViewById(R.id.et_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        progress_signup = (ProgressBar) findViewById(R.id.progress_signup);
        signUp = this;
   /*     et_confirmed_password = (EditText) findViewById(R.id.et_confirmed_password);
        et_otp = (EditText) findViewById(R.id.et_otp);
        b_sendOtp = (Button) findViewById(R.id.b_send_otp);

        b_sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usermobile = et_mobile.getText().toString();
                if (usermobile.length() != 10)
                    Toast.makeText(RegistrationApi.this, "Please enter a valid 10 digit Mobile Number", Toast.LENGTH_SHORT).show();
                else
                    new getotp().execute();
            }
        });
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {

                otp_recieived = messageText;
                if (otp.equals(otp_recieived)) {
                    et_otp.setEnabled(true);
                    et_otp.setText(messageText);
                    et_otp.setEnabled(false);
                    et_otp.setFocusable(false);
                    et_otp.setTextColor(Color.GREEN);
                    b_sendOtp.setEnabled(false);
                    b_sendOtp.setTextColor(Color.GRAY);

                }
                Log.d("Text", messageText);
                Toast.makeText(RegistrationApi.this, "OTP: " + messageText, Toast.LENGTH_LONG).show();

            }
        });
*/
        b_signup = (Button) findViewById(R.id.b_signup);
        b_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* try {
                    otp_recieived = et_otp.getText().toString();
                } catch (Exception e) {

                }*/
                username = et_name.getText().toString();
                useremail = et_email.getText().toString();
                usermobile = et_mobile.getText().toString();
                password = et_password.getText().toString();
                //confirmed_password = et_confirmed_password.getText().toString();
                if (username == null || username.equals(""))
                    Toast.makeText(SignUp.this, "Enter a username", Toast.LENGTH_SHORT).show();
                else if (!isValidEmail(et_email.getText().toString()) || et_email.getText().toString() == null || et_email.getText().toString().equals(""))
                    Toast.makeText(SignUp.this, "Enter a valid email address", Toast.LENGTH_SHORT).show();
               /* else if (!otp.equals(otp_recieived) || otp.equals(""))
                    Toast.makeText(RegistrationApi.this, "OTP incorrect", Toast.LENGTH_SHORT).show();*/
                else if (password == null || password.equals(""))
                    Toast.makeText(SignUp.this, "Enter a password", Toast.LENGTH_SHORT).show();

                else {
                    progress_signup.setVisibility(View.VISIBLE);
                    b_signup.setVisibility(View.INVISIBLE);
                    new RegistrationApi(signUp, username, useremail, usermobile, password).execute("http://13.126.131.147/Home/signup01");
                }

            }
        });

    }

   /* public class signup extends AsyncTask<String, Void, String> {
        String response;

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("name", username)
                    .add("email", useremail)
                    .add("phone",usermobile)
                    .add("password", password)
                    .build();
            Request request = new Request.Builder()
                    .url(params[0])
                    .post(formBody)
                    .build();
            try (Response response = client.newCall(request).execute()) {

                return response.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject=new JSONObject(s);
                String result=jsonObject.getString("status");
                if(result.equals("success"))
                {  *//*  final SharedPreferences sharedPreferences = Login.this.getSharedPreferences("app_settings", Login.this.MODE_PRIVATE);
                SharedPreferences.Editor editor =sharedPreferences.edit();
                editor.putBoolean("login",true);
                editor.putString("username",userid);
                editor.putString("password",password);
                editor.commit();
                okhttp.username=userid;
                okhttp.password=password;*//*
                    Intent intent = new Intent(RegistrationApi.this,MainScreen.class);
                    startActivity(intent);
                    finish();
                }
                else if(result.equals("error"))
                {   b_signup.setVisibility(View.VISIBLE);
                    progress_signup.setVisibility(View.GONE);
                    Toast.makeText(RegistrationApi.this,"Email Already Exist",Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

           *//* Toast.makeText(RegistrationApi.this, s.toString(), Toast.LENGTH_SHORT).show();
            if (s.equals("SUCCESS")) {
                final SharedPreferences sharedPreferences = RegistrationApi.this.getSharedPreferences("app_settings", RegistrationApi.this.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("login", true);
                editor.putString("username",useremail);
                editor.putString("password",password);
                editor.commit();
                okhttp.username=useremail;
                okhttp.password=password;
                Intent intent = new Intent(RegistrationApi.this, MainScreen.class);
                startActivity(intent);
                finish();
            } else {
                progress_signup.setVisibility(View.INVISIBLE);
                b_signup.setVisibility(View.VISIBLE);
            }*//*
           Log.d("value of s",s);

        }
    }*/

    public class getotp extends AsyncTask<Void, Void, String> {
        String url = null;

        String response = null;

        @Override
        protected String doInBackground(Void... params) {
            Random random = new Random();
            otp = String.format("%04d", random.nextInt(10000));
            url = "http://login.bulksmsindia.biz/messageapi.asp?username=cropscrap&password=10044991&sender=CScrap&mobile=" + usermobile + "&message=" + otp;
            try {
                response = okhttp.get(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (s.equals(" SMS Sent Successfully to - " + usermobile)) {
                    Toast.makeText(SignUp.this, "Otp has been sent to your mobile", Toast.LENGTH_SHORT).show();
                    et_mobile.setEnabled(false);
                    et_mobile.setFocusable(false);
                    et_mobile.setTextColor(Color.GREEN);
                } else {
                    throw new Exception();
                }

            } catch (Exception e) {
                Toast.makeText(SignUp.this, "There was some error. Please try again", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
