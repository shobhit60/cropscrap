package com.tech4planet.cropscrap.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.tech4planet.cropscrap.R;

import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends Activity {
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;
    public static String user_mem_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    boolean result = checkForPermission(new String[]{android.Manifest.permission.READ_CONTACTS, Manifest.permission.READ_EXTERNAL_STORAGE
                    }, PERMISSIONS_MULTIPLE_REQUEST);
                    if (result) {
                        final SharedPreferences loginPreferences = SplashScreen.this.getSharedPreferences("normallogin", MODE_PRIVATE);
                        final SharedPreferences googlePreferences = SplashScreen.this.getSharedPreferences("googleacc", MODE_PRIVATE);
                        final SharedPreferences facebookPreferences=SplashScreen.this.getSharedPreferences("facebooklogin",MODE_PRIVATE);
                        boolean normallogin = loginPreferences.getBoolean("login", false);
                        user_mem_id = loginPreferences.getString("memberid_db", "Null");
                        boolean googlelogin = googlePreferences.getBoolean("googlelogin", false);
                        boolean facebooklogin=facebookPreferences.getBoolean("fblogin",false);

                        if (normallogin || googlelogin||facebooklogin) {
                            Intent intent = new Intent(SplashScreen.this, MainScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashScreen.this, Login.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                }
            }
        };
        timerThread.start();
    }

    public boolean checkForPermission(final String[] permissions, final int permRequestCode) {
        final List<String> permissionsNeeded = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            final String perm = permissions[i];
            if (ContextCompat.checkSelfPermission(this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage(perm + " is necessary!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            permissionsNeeded.add(perm);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    // add the request.
                    permissionsNeeded.add(perm);
                }
            }
        }

        if (permissionsNeeded.size() > 0) {
            // go ahead and request permissions
            ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), permRequestCode);
            return false;
        } else {
            // no permission need to be asked so all good...we have them all.
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST:
                if (grantResults.length > 0) {
                    boolean read_contact = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_storage =grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (read_contact && read_storage) {

                        final SharedPreferences loginPreferences = SplashScreen.this.getSharedPreferences("normallogin", MODE_PRIVATE);
                        final SharedPreferences googlePreferences = SplashScreen.this.getSharedPreferences("googleacc", MODE_PRIVATE);
                        boolean normallogin = loginPreferences.getBoolean("login", false);
                        user_mem_id = loginPreferences.getString("memberid_db", "Null");
                        boolean googlelogin = googlePreferences.getBoolean("googlelogin", false);

                        if (normallogin || googlelogin) {
                            Intent intent = new Intent(SplashScreen.this, MainScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashScreen.this, Login.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                } else {
//code for deny
                }
                break;
        }
    }
}
