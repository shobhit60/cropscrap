package com.tech4planet.cropscrap.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tech4planet.cropscrap.Background.TransOweMeApi;
import com.tech4planet.cropscrap.R;

public class Transaction_Owes_Me extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction__owes__me);
        new TransOweMeApi(this).execute("http://www.cropscrap.com/admin/dutch_transaction");
    }
}
