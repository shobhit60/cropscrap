package com.tech4planet.cropscrap.Activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tech4planet.cropscrap.Adapter.ViewChequeAdapter;
import com.tech4planet.cropscrap.Background.ChequeDeletionApi;
import com.tech4planet.cropscrap.Background.FriendDeletionApi;
import com.tech4planet.cropscrap.Fragment.ViewChequeFragment;
import com.tech4planet.cropscrap.R;

public class ViewChequeDetail extends AppCompatActivity {

    TextView bank_txtview,cheque_txtview,issued_by_txtview,in_favour_of_txtview,amount_txtview,issueddate_txtview,shared_with_txtview,remark_txtview;
   Button cheque_dtl_delete;
    ImageView view_cheque_imageview;
    final String LOCATION="http://www.cropscrap.com/assets/images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cheque_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bank_txtview=(TextView)findViewById(R.id.bank_txtview);
        cheque_txtview=(TextView)findViewById(R.id.cheque_txtview);
        issued_by_txtview=(TextView)findViewById(R.id.issued_by_txtview);
        in_favour_of_txtview=(TextView)findViewById(R.id.in_favour_of_txtview);
        amount_txtview=(TextView)findViewById(R.id.amount_txtview);
        issueddate_txtview=(TextView)findViewById(R.id.issueddate_txtview);
        shared_with_txtview=(TextView)findViewById(R.id.shared_with_txtview);
        remark_txtview=(TextView)findViewById(R.id.remark_txtview);
        cheque_dtl_delete=(Button) findViewById(R.id.cheque_dtl_delete);
        view_cheque_imageview=(ImageView)findViewById(R.id.view_cheque_imageview);
        Bundle bundle=getIntent().getExtras();
        final String cheque_id=bundle.getString("cheque_id");
        String issued_date=bundle.getString("issued_date");
        String in_favour_of=bundle.getString("in_favour_of");
        String amount=bundle.getString("amount");
        String issuer_bank_name=bundle.getString("issuer_bank_name");
        String cheque_no=bundle.getString("cheque_no");
        String issued_by=bundle.getString("issued_by");
        String remark=bundle.getString("remark");
        String check_file=bundle.getString("check_file");
        final int position=bundle.getInt("position");
        bank_txtview.setText(issuer_bank_name);
        cheque_txtview.setText(cheque_no);
        issued_by_txtview.setText(issued_by);
        in_favour_of_txtview.setText(in_favour_of);
        amount_txtview.setText(amount);
        issueddate_txtview.setText(issued_date);
        remark_txtview.setText(remark);
        Picasso.with(getApplicationContext()).load(LOCATION+check_file.trim()).into(view_cheque_imageview);
        cheque_dtl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showDeleteAlert(position,cheque_id);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showDeleteAlert(final int position, final String cheque_id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new ChequeDeletionApi(cheque_id).execute("http://www.cropscrap.com/admin/delete_cheque");
                ViewChequeAdapter.cheque_id.remove(position);
                ViewChequeAdapter.issued_date.remove(position);
                ViewChequeAdapter.in_favour_of.remove(position);
                ViewChequeAdapter.amount.remove(position);
                ViewChequeAdapter.issuer_bank_name.remove(position);
                ViewChequeAdapter.cheque_no.remove(position);
                ViewChequeAdapter.issued_by.remove(position);
                ViewChequeAdapter.remark.remove(position);
                ViewChequeFragment.adapter.notifyDataSetChanged();
                dialog.dismiss();
                finish();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }
}
