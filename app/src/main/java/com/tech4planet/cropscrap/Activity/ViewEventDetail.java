package com.tech4planet.cropscrap.Activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tech4planet.cropscrap.Adapter.ViewChequeAdapter;
import com.tech4planet.cropscrap.Adapter.ViewEventRemindMeAdapter;
import com.tech4planet.cropscrap.Background.ChequeDeletionApi;
import com.tech4planet.cropscrap.Background.ReminderDeletionApi;
import com.tech4planet.cropscrap.Fragment.ViewChequeFragment;
import com.tech4planet.cropscrap.Fragment.ViewEventRemindMeFragment;
import com.tech4planet.cropscrap.R;

public class ViewEventDetail extends AppCompatActivity {

    TextView purpose_txtview,notification_txtview,fromdate_txtview,todate_txtview,remarks_txtview,sharedwith_txtview;
    Button event_dtl_delete;
    ImageView view_event_imageview;
    final String LOCATION="http://www.cropscrap.com/assets/images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        purpose_txtview=(TextView)findViewById(R.id.purpose_txtview);
        notification_txtview=(TextView)findViewById(R.id.notification_txtview);
        fromdate_txtview=(TextView)findViewById(R.id.fromdate_txtview);
        todate_txtview=(TextView)findViewById(R.id.todate_txtview);
        remarks_txtview=(TextView)findViewById(R.id.remark_txtview);
        sharedwith_txtview=(TextView)findViewById(R.id.sharedwith_txtview);
        event_dtl_delete=(Button) findViewById(R.id.event_dtl_delete);
        view_event_imageview=(ImageView)findViewById(R.id.view_event_imageview);
         Bundle bundle=getIntent().getExtras();
        String reminder_purpose=bundle.getString("reminder_purpose");
        String reminder_notification=bundle.getString("reminder_notification");
        String reminder_from_date=bundle.getString("reminder_from_date");
        String reminder_to_date=bundle.getString("reminder_to_date");
        String reminder_remarks=bundle.getString("reminder_remarks");
        String shared_with=bundle.getString("shared_with");
        String reminder_file=bundle.getString("reminder_file");
        final int position=bundle.getInt("position");
        final String reminder_id=bundle.getString("reminder_id");
        purpose_txtview.setText(reminder_purpose);
        notification_txtview.setText(reminder_notification);
        fromdate_txtview.setText(reminder_from_date);
        todate_txtview.setText(reminder_to_date);
       // sharedwith_txtview.setText(shared_with);
        Picasso.with(getApplicationContext()).load(LOCATION+reminder_file.trim()).into(view_event_imageview);
       // remarks_txtview.setText(reminder_remarks);

        event_dtl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteAlert(position,reminder_id);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showDeleteAlert(final int position, final String reminder_id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new ReminderDeletionApi(reminder_id).execute("http://www.cropscrap.com/admin/delete_reminder");
                ViewEventRemindMeAdapter.reminder_id.remove(position);
                ViewEventRemindMeAdapter.reminder_purpose.remove(position);
                ViewEventRemindMeAdapter.reminder_notification.remove(position);
                ViewEventRemindMeAdapter.reminder_from_date.remove(position);
                ViewEventRemindMeAdapter.reminder_to_date.remove(position);
                ViewEventRemindMeAdapter.reminder_remarks.remove(position);
                ViewEventRemindMeFragment.adapter.notifyDataSetChanged();
                dialog.dismiss();
                finish();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }
}
