package com.tech4planet.cropscrap.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tech4planet.cropscrap.Adapter.ViewEventRemindMeAdapter;
import com.tech4planet.cropscrap.Adapter.ViewIssueBillAdapter;
import com.tech4planet.cropscrap.Background.IssueBillDeletionApi;
import com.tech4planet.cropscrap.Background.ReminderDeletionApi;
import com.tech4planet.cropscrap.Fragment.ViewEventRemindMeFragment;
import com.tech4planet.cropscrap.Fragment.ViewIssueBillFragment;
import com.tech4planet.cropscrap.R;

import static com.tech4planet.cropscrap.Adapter.ViewEventRemindMeAdapter.reminder_id;

public class ViewIssueBillDetail extends AppCompatActivity {

    TextView name_txtview,email_txtview,mobile_txtview,attachbill_txtview;
    Button issuebill_dtl_delete;
    ImageView view_issue_bill_imageview;
    final String LOCATION="http://www.cropscrap.com/assets/images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_issuebill_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        name_txtview=(TextView)findViewById(R.id.name_txtview);
        email_txtview=(TextView)findViewById(R.id.email_txtview);
        mobile_txtview=(TextView)findViewById(R.id.mobile_txtview);
        attachbill_txtview=(TextView)findViewById(R.id.attachbill_txtview);
        issuebill_dtl_delete=(Button)findViewById(R.id.issuebill_dtl_delete);
        view_issue_bill_imageview=(ImageView)findViewById(R.id.view_issue_bill_imageview);
         Bundle bundle=getIntent().getExtras();
        final String issuebill_id=bundle.getString("issuebill_id");
        final int position=bundle.getInt("position");
        String attach_bill=bundle.getString("attach_bill");
        String customer_name=bundle.getString("customer_name");
        String customer_email_id=bundle.getString("customer_email_id");
        String customer_mobile_no=bundle.getString("customer_mobile_no");
        name_txtview.setText(customer_name);
        email_txtview.setText(customer_email_id);
        mobile_txtview.setText(customer_mobile_no);
        attachbill_txtview.setText(attach_bill);
        Picasso.with(getApplicationContext()).load(LOCATION+attach_bill.trim()).into(view_issue_bill_imageview);
       // remarks_txtview.setText(reminder_remarks);
        issuebill_dtl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteAlert(position,issuebill_id);
            }
        });

    }
    public void showDeleteAlert(final int position, final String issuebill_id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new IssueBillDeletionApi(issuebill_id).execute("http://www.cropscrap.com/admin/delete_issue_bill");
                ViewIssueBillAdapter.id.remove(position);
                ViewIssueBillAdapter.attach_bill.remove(position);
                ViewIssueBillAdapter.customer_name.remove(position);
                ViewIssueBillAdapter.customer_email_id.remove(position);
                ViewIssueBillAdapter.customer_mobile_no.remove(position);
                ViewIssueBillFragment.adapter.notifyDataSetChanged();
                dialog.dismiss();
                finish();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
