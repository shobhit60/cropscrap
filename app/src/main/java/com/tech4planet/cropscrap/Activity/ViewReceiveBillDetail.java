package com.tech4planet.cropscrap.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tech4planet.cropscrap.Adapter.ViewIssueBillAdapter;
import com.tech4planet.cropscrap.Adapter.ViewReceiveBillAdapter;
import com.tech4planet.cropscrap.Background.IssueBillDeletionApi;
import com.tech4planet.cropscrap.Background.ReceiveBillDeletionApi;
import com.tech4planet.cropscrap.Fragment.ViewIssueBillFragment;
import com.tech4planet.cropscrap.Fragment.ViewReceiveBillFragment;
import com.tech4planet.cropscrap.R;

public class ViewReceiveBillDetail extends AppCompatActivity {

    TextView name_txtview,category_txtview,date_txtview,view_receivebill_dtl_remark_txtview,sharedwith_txtview;
    Button receivebill_dtl_delete;
    ImageView view_receive_bill_imageview;
    final String LOCATION="http://www.cropscrap.com/assets/images/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_receivebill_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        name_txtview=(TextView)findViewById(R.id.name_txtview);
        category_txtview=(TextView)findViewById(R.id.category_txtview);
        date_txtview=(TextView)findViewById(R.id.date_txtview);
        view_receivebill_dtl_remark_txtview=(TextView)findViewById(R.id.view_receivebill_dtl_remark_txtview);
        sharedwith_txtview=(TextView)findViewById(R.id.sharedwith_txtview);
        receivebill_dtl_delete=(Button)findViewById(R.id.receivebill_dtl_delete);
        view_receive_bill_imageview=(ImageView)findViewById(R.id.view_receive_bill_imageview);
         Bundle bundle=getIntent().getExtras();
        String seller_name=bundle.getString("seller_name");
        String category=bundle.getString("category");
        String date=bundle.getString("date");
        String remark=bundle.getString("remark");
        String attach_bill=bundle.getString("attach_bill");
        final String receivebill_id=bundle.getString("receivebill_id");
        final int position=bundle.getInt("position");
        name_txtview.setText(seller_name);
        category_txtview.setText(category);
        date_txtview.setText(date);
        view_receivebill_dtl_remark_txtview.setText(remark);
        Picasso.with(getApplicationContext()).load(LOCATION+attach_bill.trim()).into(view_receive_bill_imageview);
       // remarks_txtview.setText(reminder_remarks);
        receivebill_dtl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteAlert(position,receivebill_id);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showDeleteAlert(final int position, final String receivebill_id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new ReceiveBillDeletionApi(receivebill_id).execute("http://www.cropscrap.com/admin/delete_received_bill");
                ViewReceiveBillAdapter.receivebill_id.remove(position);
                ViewReceiveBillAdapter.seller_name.remove(position);
                ViewReceiveBillAdapter.category.remove(position);
                ViewReceiveBillAdapter.date.remove(position);
                ViewReceiveBillAdapter.remark.remove(position);
                ViewReceiveBillFragment.adapter.notifyDataSetChanged();
                dialog.dismiss();
                finish();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }


}
