package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 17/11/17.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyView> {

    static ArrayList<ContactModel> mylist;
    Activity activity;
    private int lastSelectedPosition = -1;

    public ContactAdapter(Activity activity, ArrayList<ContactModel> mylist) {
        this.activity = activity;
        this.mylist = mylist;
    }

    public static void filter(ArrayList<ContactModel> newlist) {

        mylist = new ArrayList<ContactModel>();
        mylist.addAll(newlist);

    }

    @Override
    public ContactAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_adapter_layout, parent, false);
        MyView myview = new MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final ContactAdapter.MyView holder, final int position) {

        holder.username.setText(mylist.get(position).getName());
        holder.userphone.setText(mylist.get(position).getNumber());
      /*  holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                holder.linearLayout.setBackgroundColor(Color.parseColor("green"));
                return true;
            }
        });*/
        holder.checkBox.setChecked(mylist.get(position).isSelected());
        holder.checkBox.setTag(mylist.get(position));
       /* holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

          }
      });*/

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ContactModel contactModel=(ContactModel)cb.getTag();
                contactModel.setSelected(cb.isChecked());
                mylist.get(position).setSelected(cb.isChecked());
               /* if (v.isSelected()) {
                    //holder.checkBox.setVisibility(View.GONE);
                    mylist.get(position).setSelected(false);
                    Log.d("checkbox", "unchecked");
                } else {
                    mylist.get(position).setSelected(true);
                    //holder.checkBox.setVisibility(View.GONE);
                    Log.d("checkbox", "checked");
                }*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    public class MyView extends RecyclerView.ViewHolder {

        TextView username, userphone;
        LinearLayout linearLayout;
        CheckBox checkBox;

        public MyView(View itemView) {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.name);
            userphone = (TextView) itemView.findViewById(R.id.phone);
            //linearLayout=(LinearLayout)itemView.findViewById(R.id.linearlayout);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            this.setIsRecyclable(false);
           /* lastSelectedPosition=getAdapterPosition();
            Log.d("lastselectedposition",lastSelectedPosition+"");*/
           /* radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                   // notifyDataSetChanged();
                 *//*if(radioButton.isChecked())
                 {
                     radioButton.setChecked(false);
                 }
                 else
                     radioButton.setChecked(true);*//*

                }
            });*/
        }
    }

}
