package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 23/11/17.
 */

public class ContactFilterAdapter extends RecyclerView.Adapter<ContactFilterAdapter.MyView> {

    static ArrayList<ContactModel> mylist;
    Activity activity;
    private int lastSelectedPosition = -1;

    public ContactFilterAdapter(Activity activity, ArrayList<ContactModel> mylist) {
        this.activity = activity;
        ContactFilterAdapter.mylist = mylist;

    }

    @Override
    public ContactFilterAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_filter_layout, parent, false);
        ContactFilterAdapter.MyView myview = new ContactFilterAdapter.MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final ContactFilterAdapter.MyView holder, final int position) {

        holder.username.setText(mylist.get(position).getName());
        holder.userphone.setText(mylist.get(position).getNumber());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mylist.get(position).setSelected(false);
                mylist.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    public class MyView extends RecyclerView.ViewHolder {

        TextView username, userphone;
        LinearLayout linearLayout;
        ImageView delete;

        public MyView(View itemView) {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.name);
            userphone = (TextView) itemView.findViewById(R.id.phone);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            this.setIsRecyclable(false);

        }
    }


}
