package com.tech4planet.cropscrap.Adapter;

/**
 * Created by tech4planet on 27/11/17.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech4planet.cropscrap.Background.GroupDeletionApi;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyView> {

    Activity activity;
    ArrayList<String> grouplist, groupid;

    public ContactListAdapter(Activity activity, ArrayList<String> grouplist, ArrayList<String> groupid) {
        this.activity = activity;
        this.grouplist = grouplist;
        this.groupid = groupid;
    }

    @Override
    public ContactListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_adapter_layout, parent, false);
        ContactListAdapter.MyView myview = new ContactListAdapter.MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final ContactListAdapter.MyView holder, final int position) {

        holder.grp_name.setText(grouplist.get(position).toString());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GroupDeletionApi(activity, groupid.get(position).toString()).execute("http://35.154.121.101/admin/delete_group/");
            }
        });


    }

    @Override
    public int getItemCount() {
        return grouplist.size();
    }

    public class MyView extends RecyclerView.ViewHolder {

        TextView grp_name, userphone;
        ImageView delete;

        public MyView(View itemView) {
            super(itemView);
            grp_name = (TextView) itemView.findViewById(R.id.grp_name);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            //this.setIsRecyclable(false);

        }
    }


}
