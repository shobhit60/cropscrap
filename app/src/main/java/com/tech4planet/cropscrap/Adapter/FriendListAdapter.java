package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech4planet.cropscrap.Background.FriendDeletionApi;
import com.tech4planet.cropscrap.Background.ViewChequeApi;
import com.tech4planet.cropscrap.Fragment.FriendListFragment;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 30/11/17.
 */

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.MyView> {

    Activity activity;
    ArrayList<String> friendname, friendmob, friendid;

    public FriendListAdapter(Activity activity, ArrayList<String> friendname, ArrayList<String> friendmob, ArrayList<String> friendid) {
        this.activity = activity;
        this.friendname = friendname;
        this.friendmob = friendmob;
        this.friendid = friendid;
    }

    @Override
    public FriendListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list_adapter_layout, parent, false);
        FriendListAdapter.MyView myview = new FriendListAdapter.MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final FriendListAdapter.MyView holder, final int position) {

        holder.friend_name.setText(friendname.get(position).toString());
        holder.friend_mob.setText(friendmob.get(position).toString());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDeleteAlert(position);

            }
        });



    }

    @Override
    public int getItemCount() {
        return friendid.size();
    }

    public class MyView extends RecyclerView.ViewHolder {

        TextView friend_name, friend_mob;
        ImageView delete;

        public MyView(View itemView) {
            super(itemView);
            friend_name = (TextView) itemView.findViewById(R.id.friend_name);
            friend_mob = (TextView) itemView.findViewById(R.id.friend_mob);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            //this.setIsRecyclable(false);


        }
    }

    public void showDeleteAlert(final int position){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new FriendDeletionApi(activity, friendid.get(position).toString()).execute("http://13.126.131.147/admin/delete_friend_list/");
                friendid.remove(position);
                friendmob.remove(position);
                friendname.remove(position);
                notifyDataSetChanged();
                dialog.dismiss();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }
}
