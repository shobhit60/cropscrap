package com.tech4planet.cropscrap.Adapter;

/**
 * Created by tech4planet on 27/11/17.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.GroupDetail;
import com.tech4planet.cropscrap.Background.FriendDeletionApi;
import com.tech4planet.cropscrap.Background.GroupDeletionApi;
import com.tech4planet.cropscrap.Background.GroupDetailApi;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.MyView> {

    static Activity activity;
    ArrayList<String> grouplist, groupid;

    public GroupListAdapter(Activity activity, ArrayList<String> grouplist, ArrayList<String> groupid) {
        GroupListAdapter.activity = activity;
        this.grouplist = grouplist;
        this.groupid = groupid;
    }

    @Override
    public GroupListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list_adapter_layout, parent, false);
        GroupListAdapter.MyView myview = new GroupListAdapter.MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final GroupListAdapter.MyView holder, final int position) {

        holder.grp_name.setText(grouplist.get(position).toString());

        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent intent=new Intent(activity,GroupDetail.class);
                intent.putExtra("grp_id",groupid.get(position).toString());
                activity.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteAlert(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return grouplist.size();
    }

    public static class MyView extends RecyclerView.ViewHolder {

        TextView grp_name;
        ImageView delete;
        Button btn_detail;
        LinearLayout grp_detail_list_ll;
        RecyclerView.LayoutManager layoutManager;
        RecyclerView grp_lst_detail_recyclerview;
        RecyclerView.Adapter adapter;

        public MyView(View itemView) {
            super(itemView);
            grp_name = (TextView) itemView.findViewById(R.id.grp_name);
            btn_detail = (Button) itemView.findViewById(R.id.btn_detail);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            grp_detail_list_ll = (LinearLayout) itemView.findViewById(R.id.grp_detail_list_ll);
            grp_lst_detail_recyclerview = (RecyclerView) itemView.findViewById(R.id.grp_lst_detail_recyclerview);
            layoutManager = new LinearLayoutManager(activity);
            grp_lst_detail_recyclerview.setLayoutManager(layoutManager);
            grp_lst_detail_recyclerview.setHasFixedSize(true);
            //this.setIsRecyclable(false);

        }
    }


    public void showDeleteAlert(final int position){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new GroupDeletionApi(activity, groupid.get(position).toString()).execute("http://www.cropscrap.com/admin/delete_group/");
                groupid.remove(position);
                grouplist.remove(position);
                notifyDataSetChanged();
                dialog.dismiss();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }
}
