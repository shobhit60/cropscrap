package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 16/12/17.
 */

public class GroupListDetailAdapter extends RecyclerView.Adapter<GroupListDetailAdapter.GroupDetailView> {

    public static Activity activity;
    ArrayList<String> mem_name, mem_mob;

    public GroupListDetailAdapter(Activity activity, ArrayList<String> mem_name, ArrayList<String> mem_mob) {
        GroupListDetailAdapter.activity = activity;
        this.mem_name = mem_name;
        this.mem_mob = mem_mob;
        Log.d("value of mem_name", mem_name.toString());
        Log.d("value of mem_mob", mem_mob.toString());
    }

    @Override
    public GroupListDetailAdapter.GroupDetailView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list_detail_adapter_layout, parent, false);
        GroupListDetailAdapter.GroupDetailView myview = new GroupListDetailAdapter.GroupDetailView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final GroupListDetailAdapter.GroupDetailView holder, final int position) {

        holder.name.setText(mem_name.get(position).toString());
        holder.phone.setText(mem_mob.get(position).toString());


    }

    @Override
    public int getItemCount() {
        return mem_name.size();
    }

    public class GroupDetailView extends RecyclerView.ViewHolder {

        TextView name, phone;

        public GroupDetailView(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            phone = (TextView) itemView.findViewById(R.id.phone);

            //this.setIsRecyclable(false);

        }
    }


}
