package com.tech4planet.cropscrap.Adapter;

/**
 * Created by shobhit on 24-Jan-18.
 */
import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tech4planet.cropscrap.Background.GroupDetailApi;
import com.tech4planet.cropscrap.Fragment.DutchIt_SharedBill_Fragment;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

public class SharedBillGroupListAdapter extends RecyclerView.Adapter<SharedBillGroupListAdapter.MyView> {

    static Activity activity;
    ArrayList<String> grouplist,groupid;

    public SharedBillGroupListAdapter(Activity activity, ArrayList<String> grouplist, ArrayList<String> groupid) {
        this.activity = activity;
        this.grouplist = grouplist;
        this.groupid=groupid;
    }

    @Override
    public SharedBillGroupListAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shared_bill_adapter_layout, parent, false);
        SharedBillGroupListAdapter.MyView myview = new SharedBillGroupListAdapter.MyView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final SharedBillGroupListAdapter.MyView holder, final int position) {

        holder.textview.setText(grouplist.get(position).toString());

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int share_evenly_ID = DutchIt_SharedBill_Fragment.share_evenly_rg.getCheckedRadioButtonId();
                RadioButton rb_share_evenly=(RadioButton)DutchIt_SharedBill_Fragment.share_evenly_rg.findViewById(share_evenly_ID);
                int share_rg_ID = DutchIt_SharedBill_Fragment.share_rg.getCheckedRadioButtonId();
                RadioButton rb_share=(RadioButton)DutchIt_SharedBill_Fragment.share_rg.findViewById(share_rg_ID);
                String share_evenly=rb_share_evenly.getText().toString().trim();
                String share=rb_share.getText().toString().trim();
                if(share.equals("Share Evenly") && share_evenly.equals("Share With Group"))
                {
                    DutchIt_SharedBill_Fragment.shareddetailevenlyrecyclerview.setVisibility(View.VISIBLE);
                    new GroupDetailApi(activity,groupid.get(position),"shared_bill_fragment_with_evenly", DutchIt_SharedBill_Fragment.shareddetailevenlyrecyclerview,DutchIt_SharedBill_Fragment.sharedetailevenlydadapter).execute("http://13.126.131.147/admin/group_details/");
                    DutchIt_SharedBill_Fragment.grp_dtl_list_ll.setVisibility(View.VISIBLE);
                }

                else
                {
                    DutchIt_SharedBill_Fragment.shareddetailunevenlyrecyclerview.setVisibility(View.VISIBLE);
                    new GroupDetailApi(activity,groupid.get(position),"shared_bill_fragment_with_unevenly", DutchIt_SharedBill_Fragment.shareddetailunevenlyrecyclerview,DutchIt_SharedBill_Fragment.sharedetailunevenlydadapter).execute("http://13.126.131.147/admin/group_details/");
                    DutchIt_SharedBill_Fragment.grp_dtl_list_unevenly_ll.setVisibility(View.VISIBLE);
                }


            }
        });



    }

    @Override
    public int getItemCount() {
        return grouplist.size();
    }

    public static class MyView extends RecyclerView.ViewHolder {

        TextView textview;
        Button button;

        public MyView(View itemView) {
            super(itemView);
            textview = (TextView) itemView.findViewById(R.id.textview);
            button = (Button) itemView.findViewById(R.id.button);
            //this.setIsRecyclable(false);

        }
    }


    /*public void showDeleteAlert(final int position){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Delete Alert!!");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete record");

        // On pressing Settings button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do your work here
                new GroupDeletionApi(activity, groupid.get(position).toString()).execute("http://www.cropscrap.com/admin/delete_group/");
                groupid.remove(position);
                grouplist.remove(position);
                notifyDataSetChanged();
                dialog.dismiss();

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });


        alertDialog.setCancelable(true);
        // Showing Alert Message
        alertDialog.show();
    }*/
}
