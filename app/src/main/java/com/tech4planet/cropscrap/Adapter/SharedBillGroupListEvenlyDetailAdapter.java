package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

import static com.tech4planet.cropscrap.Fragment.DutchIt_SharedBill_Fragment.mem_amt;

/**
 * Created by tech4planet on 16/12/17.
 */

public class SharedBillGroupListEvenlyDetailAdapter extends RecyclerView.Adapter<SharedBillGroupListEvenlyDetailAdapter.GroupDetailView> {

    public static Activity activity;
    ArrayList<String> mem_name;
    //public static ArrayList<String> mem_amt;
    private String[] mDataset;
    public SharedBillGroupListEvenlyDetailAdapter(Activity activity, ArrayList<String> mem_name) {
        SharedBillGroupListEvenlyDetailAdapter.activity = activity;
        this.mem_name = mem_name;
       // mem_amt=new ArrayList<>();
        mem_amt=new String[mem_name.size()];
    }

    @Override
    public SharedBillGroupListEvenlyDetailAdapter.GroupDetailView onCreateViewHolder(ViewGroup parent, int viewType) {

       View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shared_bill_grp_dtl_evenly_adapter_layout, parent, false);

        SharedBillGroupListEvenlyDetailAdapter.GroupDetailView myview = new SharedBillGroupListEvenlyDetailAdapter.GroupDetailView(v);
        return myview;
    }

    @Override
    public void onBindViewHolder(final SharedBillGroupListEvenlyDetailAdapter.GroupDetailView holder, final int position) {

        holder.textview.setText(mem_name.get(position).toString());
        holder.amt_editxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               /* mem_amt.add(position,s.toString());
                Log.d("value of mem_amt",mem_amt.get(position));
                Log.d("mem_amt sie",mem_amt.size()+"");*/
                mem_amt[holder.getAdapterPosition()]=s.toString();
                /*Log.d("mem_amt value",mem_amt[holder.getAdapterPosition()]);
                Log.d("mem_amt length",mem_amt.length+"");*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return mem_name.size();
    }

    public class GroupDetailView extends RecyclerView.ViewHolder {

        TextView textview;
        EditText amt_editxt;
        public GroupDetailView(View itemView) {
            super(itemView);
            textview = (TextView) itemView.findViewById(R.id.textview);
            amt_editxt=(EditText)itemView.findViewById(R.id.amt_editxt);
            //this.setIsRecyclable(false);

        }
    }


}
