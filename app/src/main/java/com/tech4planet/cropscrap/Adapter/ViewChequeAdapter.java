package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.ViewChequeDetail;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 8/12/17.
 */

public class ViewChequeAdapter extends RecyclerView.Adapter<ViewChequeAdapter.ChequeView> {

    Activity activity;
    public static ArrayList<String> cheque_id, issued_date, in_favour_of, amount, issuer_bank_name,cheque_no,issued_by,remark,check_file;
    public ViewChequeAdapter(Activity activity,ArrayList<String> cheque_id, ArrayList<String> issued_date, ArrayList<String> in_favour_of, ArrayList<String> amount, ArrayList<String> issuer_bank_name,ArrayList<String> cheque_no,ArrayList<String> issued_by,ArrayList<String> remark, ArrayList<String> check_file) {
        this.activity = activity;
        this.cheque_id=cheque_id;
        this.issued_date = issued_date;
        this.in_favour_of = in_favour_of;
        this.amount = amount;
        this.issuer_bank_name = issuer_bank_name;
        this.cheque_no=cheque_no;
        this.issued_by=issued_by;
        this.remark=remark;
        this.check_file=check_file;
      //  Log.d("value of in_favour_of", in_favour_of.get(0));
    }


    @Override
    public ViewChequeAdapter.ChequeView onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cheque_adapter_layout, parent, false);
        ChequeView chequeView = new ChequeView(view);
        return chequeView;
    }

    @Override
    public void onBindViewHolder(ViewChequeAdapter.ChequeView holder, final int position) {
        holder.date01.setText(issued_date.get(position));
        holder.in_favour_of01.setText(in_favour_of.get(position));
        holder.amount01.setText(amount.get(position));
        holder.view_cheque_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity,ViewChequeDetail.class);
                intent.putExtra("cheque_id",cheque_id.get(position));
                intent.putExtra("issued_date",issued_date.get(position));
                intent.putExtra("in_favour_of",in_favour_of.get(position));
                intent.putExtra("amount",amount.get(position));
                intent.putExtra("issuer_bank_name",issuer_bank_name.get(position));
                intent.putExtra("cheque_no",cheque_no.get(position));
                intent.putExtra("issued_by",issued_by.get(position));
                intent.putExtra("remark",remark.get(position));
                intent.putExtra("position",position);
                intent.putExtra("check_file",check_file.get(position));
            activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return issued_date.size();
    }

    class ChequeView extends RecyclerView.ViewHolder {
        TextView date01, in_favour_of01, amount01;
        CardView view_cheque_cardview;
        public ChequeView(View itemView) {
            super(itemView);
            date01 = (TextView) itemView.findViewById(R.id.date01);
            in_favour_of01 = (TextView) itemView.findViewById(R.id.in_favour_of01);
            amount01 = (TextView) itemView.findViewById(R.id.amount01);
            view_cheque_cardview=(CardView)itemView.findViewById(R.id.view_cheque_cardview);
        }
    }
}
