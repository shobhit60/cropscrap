package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.ViewEventDetail;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 15/12/17.
 */

public class ViewEventRemindMeAdapter extends RecyclerView.Adapter<ViewEventRemindMeAdapter.ReminderView> {

    Activity activity;
   public static ArrayList<String>  reminder_id, reminder_purpose,reminder_notification, reminder_from_date, reminder_to_date,reminder_remarks,shared_with,reminder_file;

    public ViewEventRemindMeAdapter(Activity activity,ArrayList<String> reminder_id, ArrayList<String> reminder_purpose,ArrayList<String> reminder_notification, ArrayList<String> reminder_from_date, ArrayList<String> reminder_to_date,ArrayList<String> reminder_remarks, ArrayList<String> shared_with,ArrayList<String> reminder_file) {
        this.activity = activity;
        this.reminder_id=reminder_id;
        this.reminder_purpose = reminder_purpose;
        this.reminder_notification=reminder_notification;
        this.reminder_from_date = reminder_from_date;
        this.reminder_to_date = reminder_to_date;
        this.reminder_remarks = reminder_remarks;
        this.shared_with=shared_with;
        this.reminder_file=reminder_file;
    }


    @Override
    public ViewEventRemindMeAdapter.ReminderView onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_remindme_adapter_layout, parent, false);
        ViewEventRemindMeAdapter.ReminderView remindView = new ViewEventRemindMeAdapter.ReminderView(view);
        return remindView;
    }

    @Override
    public void onBindViewHolder(ViewEventRemindMeAdapter.ReminderView holder, final int position) {

        holder.purpose.setText(reminder_purpose.get(position));
        holder.from_date.setText(reminder_from_date.get(position));
        holder.to_date.setText(reminder_to_date.get(position));
        holder.view_event_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity,ViewEventDetail.class);
                intent.putExtra("reminder_purpose",reminder_purpose.get(position));
                intent.putExtra("reminder_notification",reminder_notification.get(position));
                intent.putExtra("reminder_from_date",reminder_from_date.get(position));
                intent.putExtra("reminder_to_date",reminder_to_date.get(position));
                intent.putExtra("reminder_remarks",reminder_remarks.get(position));
                intent.putExtra("reminder_id",reminder_id.get(position));
                intent.putExtra("position",position);
                intent.putExtra("reminder_file",reminder_file.get(position));
                //intent.putExtra("shared_with",shared_with.get(position));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return reminder_purpose.size();
    }

    class ReminderView extends RecyclerView.ViewHolder {
        TextView purpose, from_date, to_date;
        CardView view_event_cardview;
        public ReminderView(View itemView) {
            super(itemView);
            purpose = (TextView) itemView.findViewById(R.id.purpose);
            from_date = (TextView) itemView.findViewById(R.id.from_date);
            to_date = (TextView) itemView.findViewById(R.id.to_date);
            view_event_cardview=(CardView)itemView.findViewById(R.id.view_event_cardview);
        }
    }
}
