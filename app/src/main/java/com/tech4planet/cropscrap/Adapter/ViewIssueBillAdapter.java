package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.ViewIssueBillDetail;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 11/12/17.
 */

public class ViewIssueBillAdapter extends RecyclerView.Adapter<ViewIssueBillAdapter.ChequeView> {

    Activity activity;
    public static ArrayList<String> id, attach_bill, customer_name, customer_email_id, customer_mobile_no;

    public ViewIssueBillAdapter(Activity activity, ArrayList<String> id, ArrayList<String> attach_bill, ArrayList<String> customer_name, ArrayList<String> customer_email_id, ArrayList<String> customer_mobile_no) {
        this.activity = activity;
        this.id = id;
        this.attach_bill = attach_bill;
        this.customer_name = customer_name;
        this.customer_email_id = customer_email_id;
        this.customer_mobile_no = customer_mobile_no;
    }


    @Override
    public ViewIssueBillAdapter.ChequeView onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_issue_bill_adapter_layout, parent, false);
        ViewIssueBillAdapter.ChequeView chequeView = new ViewIssueBillAdapter.ChequeView(view);
        return chequeView;
    }

    @Override
    public void onBindViewHolder(ViewIssueBillAdapter.ChequeView holder, final int position) {

        holder.name.setText(customer_name.get(position));
        holder.email.setText(customer_email_id.get(position));
        holder.mob.setText(customer_mobile_no.get(position));
        holder.view_issuebill_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, ViewIssueBillDetail.class);
                intent.putExtra("attach_bill",attach_bill.get(position));
                intent.putExtra("customer_name",customer_name.get(position));
                intent.putExtra("customer_email_id",customer_email_id.get(position));
                intent.putExtra("customer_mobile_no",customer_mobile_no.get(position));
                intent.putExtra("issuebill_id",id.get(position));
                intent.putExtra("position",position);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return id.size();
    }

    class ChequeView extends RecyclerView.ViewHolder {
        TextView name, email, mob;
        CardView view_issuebill_cardview;
        public ChequeView(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email);
            mob = (TextView) itemView.findViewById(R.id.mob);
            view_issuebill_cardview=(CardView)itemView.findViewById(R.id.view_issuebill_cardview);
        }
    }
}
