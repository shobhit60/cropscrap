package com.tech4planet.cropscrap.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.ViewReceiveBillDetail;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * Created by tech4planet on 12/12/17.
 */

public class ViewReceiveBillAdapter extends RecyclerView.Adapter<ViewReceiveBillAdapter.ChequeView> {

    Activity activity;
    public static ArrayList<String> receivebill_id,seller_name, category, date, remark,attach_bill;

    public ViewReceiveBillAdapter(Activity activity,ArrayList<String> receivebill_id, ArrayList<String> seller_name, ArrayList<String> category, ArrayList<String> date, ArrayList<String> remark, ArrayList<String> attach_bill) {
        this.activity = activity;
        this.receivebill_id=receivebill_id;
        this.seller_name = seller_name;
        this.category = category;
        this.date = date;
        this.remark=remark;
        this.attach_bill=attach_bill;
    }


    @Override
    public ViewReceiveBillAdapter.ChequeView onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_receive_bill_adapter_layout, parent, false);
        ViewReceiveBillAdapter.ChequeView chequeView = new ViewReceiveBillAdapter.ChequeView(view);
        return chequeView;
    }

    @Override
    public void onBindViewHolder(ViewReceiveBillAdapter.ChequeView holder, final int position) {

        holder.name.setText(seller_name.get(position));
        holder.category.setText(category.get(position));
        holder.date.setText(date.get(position));
        holder.view_receive_bill_adapter_recyclerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(activity, ViewReceiveBillDetail.class);
                intent.putExtra("seller_name",seller_name.get(position));
                intent.putExtra("category",category.get(position));
                intent.putExtra("date",date.get(position));
                intent.putExtra("remark",remark.get(position));
                intent.putExtra("receivebill_id",receivebill_id.get(position));
                intent.putExtra("position",position);
                intent.putExtra("attach_bill",attach_bill.get(position));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return seller_name.size();
    }

    class ChequeView extends RecyclerView.ViewHolder {
        TextView name, category, date;
        CardView view_receive_bill_adapter_recyclerview;
        public ChequeView(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            category = (TextView) itemView.findViewById(R.id.category);
            date = (TextView) itemView.findViewById(R.id.date);
            view_receive_bill_adapter_recyclerview=(CardView)itemView.findViewById(R.id.view_receive_bill_adapter_recyclerview);
        }
    }
}
