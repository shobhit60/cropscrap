package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.tech4planet.cropscrap.Adapter.GroupListDetailAdapter.activity;

/**
 * Created by tech4planet on 6/12/17.
 */

public class AddChequeApi extends AsyncTask<String, Void, String> {
    String bank_spinner_text, cheque_no_text, issuer_name_text, receiver_name_text, amt_text;
    String shareradiogroup_text, friend_id_text, group_id_text,issueddate_text;
    File file;
    ProgressDialog progressDialog;
    Activity activity;
    public AddChequeApi(Activity activity, String bank_spinner_text, String cheque_no_text, String issuer_name_text, String receiver_name_text, String amt_text, String issueddate_text, File file, String shareradiogroup_text, String friend_id_text, String group_id_text) {
        this.activity=activity;
        this.bank_spinner_text = bank_spinner_text;
        this.cheque_no_text = cheque_no_text;
        this.issuer_name_text = issuer_name_text;
        this.receiver_name_text = receiver_name_text;
        this.amt_text = amt_text;
        this.issueddate_text=issueddate_text;
        this.file=file;
        this.shareradiogroup_text = shareradiogroup_text;
        this.friend_id_text = friend_id_text;
        this.group_id_text = group_id_text;
        progressDialog = new ProgressDialog(activity);
    }

        @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("issuer_bank_name", bank_spinner_text)
                .addFormDataPart("cheque_no", cheque_no_text)
                .addFormDataPart("issued_by", issuer_name_text)
                .addFormDataPart("in_favour_of", receiver_name_text)
                .addFormDataPart("amount", amt_text)
                .addFormDataPart("issued_date", issueddate_text)
                .addFormDataPart("check_file",file.getName(), RequestBody.create(MediaType.parse("image/*"),file))
                .addFormDataPart("remark", "tourist")
                .addFormDataPart("action[0]", shareradiogroup_text)
                .addFormDataPart("friend_id", friend_id_text)
                .addFormDataPart("group_id", group_id_text);
        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPostExecute(String addchequestring) {

        super.onPostExecute(addchequestring);

        if (addchequestring==null)
        {
            Log.d("value of addcheque", "no value coming from server");
            progressDialog.cancel();
        }
        else
        {
             String status=null;
            Log.d("addchequestring", addchequestring);
            try {
                JSONObject jsonObject=new JSONObject(addchequestring);
                status=jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(status.equals("success"))
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Cheque Added Succcessfully",Toast.LENGTH_LONG).show();
            }
            else
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
            }



        }


    }
}