package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 15/12/17.
 */

public class AddReminderApi extends AsyncTask<String, Void, String> {
    String remindme_text, location_text, fromcalender_text, tocalender_text,datecalender_text, start_time_text, end_time_text, recurring_id_text, set_alertid_text;
    String remarks_text,friend_id_text, group_id_text;
    String reminder_rg_text,single_day_rg_text,share_rg_text,recurring_rg_text;
    Activity activity;
    ProgressDialog progressDialog;
    File file;

    public AddReminderApi(Activity activity, String remindme_text, String location_text, String reminder_rg_text, String single_day_rg_text, String fromcalender_text, String tocalender_text, String datecalender_text, String start_time_text, String end_time_text,File file,String recurring_rg_text, String recurring_id_text, String set_alertid_text, String remarks_text, String share_rg_text, String friend_id_text, String group_id_text) {
        this.activity = activity;
        this.remindme_text = remindme_text;
        this.location_text = location_text;
        this.reminder_rg_text=reminder_rg_text;
        this.single_day_rg_text=single_day_rg_text;
        this.fromcalender_text = fromcalender_text;
        this.tocalender_text = tocalender_text;
        this.datecalender_text=datecalender_text;
        this.start_time_text=start_time_text;
        this.end_time_text=end_time_text;
        this.file=file;
        this.recurring_rg_text=recurring_rg_text;
        this.recurring_id_text = recurring_id_text;
        this.set_alertid_text = set_alertid_text;
        this.remarks_text = remarks_text;
        this.share_rg_text = share_rg_text;
        this.friend_id_text = friend_id_text;
        this.group_id_text = group_id_text;
        progressDialog = new ProgressDialog(activity);
    }
    /*@Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }*/

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("reminder_file", file.getName(), RequestBody.create(MediaType.parse("image/*"),file))
                .addFormDataPart("eventis",reminder_rg_text)
                .addFormDataPart("select-date",single_day_rg_text)
                .addFormDataPart("issued_date",datecalender_text)
                .addFormDataPart("daterange",fromcalender_text+"-"+tocalender_text)
                .addFormDataPart("pickatime",start_time_text)
                .addFormDataPart("pickatime2",end_time_text)
                .addFormDataPart("rec",recurring_rg_text)
                .addFormDataPart("reminder_purpose", remindme_text)
                .addFormDataPart("reminder_location", location_text)
                .addFormDataPart("reminder_custom_date", set_alertid_text)
                .addFormDataPart("reminder_custom_date1",recurring_id_text)
                .addFormDataPart("reminder_remarks", remarks_text)
                .addFormDataPart("action[0]", share_rg_text)
                .addFormDataPart("friend_id", friend_id_text)
                .addFormDataPart("group_id", group_id_text);

        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String addreminder) {

        super.onPostExecute(addreminder);
        Log.d("value of addreminder", addreminder);
       /* if (addreminder.isEmpty()) {
            Log.d("value of addreminder", "no value coming from server");
            progressDialog.cancel();
        } else {
            String status=null;
            Log.d("value of addreminder", addreminder);
            try {
                JSONObject jsonObject=new JSONObject(addreminder);
                status=jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(status.equals("success"))
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Reminder Added Succcessfully",Toast.LENGTH_LONG).show();
            }
            else
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
            }


        }*/

    }
}
