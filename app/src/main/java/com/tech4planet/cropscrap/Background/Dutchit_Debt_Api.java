package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 19/12/17.
 */

public class Dutchit_Debt_Api extends AsyncTask<String, Void, String> {
    String select_dept_rg_text, owestext, purposestring, issued_date_text;
    String amountstring, currencytext, notestring;
    File file;
    ProgressDialog progressDialog;
    Activity activity;
    public Dutchit_Debt_Api(Activity activity,String select_dept_rg_text,String owestext, String purposestring, String issued_date_text,String amountstring,String currencytext, String notestring, File file) {
        this.activity=activity;
        this.select_dept_rg_text = select_dept_rg_text;
        this.owestext=owestext;
        this.purposestring = purposestring;
        this.issued_date_text = issued_date_text;
        this.amountstring=amountstring;
        this.file=file;
        this.currencytext = currencytext;
        this.notestring = notestring;
        progressDialog=new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("purpose", purposestring)
                .addFormDataPart("amount", amountstring)
                .addFormDataPart("currency", currencytext)
                .addFormDataPart("whathappened", select_dept_rg_text)
                .addFormDataPart("issued_date", issued_date_text)
                .addFormDataPart("bill_file",file.getName(), RequestBody.create(MediaType.parse("image"),file))
                .addFormDataPart("owes", owestext)
                .addFormDataPart("notes", notestring);

        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPostExecute(String dutchitdebtstring) {

        super.onPostExecute(dutchitdebtstring);

        if (dutchitdebtstring==null)
        {
            Log.d("dutchitdebtstring", "no value coming from server");
            progressDialog.cancel();
        }
        else
        {
            String status=null;
            Log.d("dutchitdebtstring", dutchitdebtstring);
            try {
                JSONObject jsonObject=new JSONObject(dutchitdebtstring);
                status=jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(status.equals("success"))
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Cheque Added Succcessfully",Toast.LENGTH_LONG).show();
            }
            else
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
            }



        }


    }
}