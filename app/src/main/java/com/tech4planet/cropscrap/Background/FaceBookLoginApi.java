package com.tech4planet.cropscrap.Background;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.Login;
import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by shobhit on 08-Jan-18.
 */

public class FaceBookLoginApi extends AsyncTask<String, Void, String> {

    String id,name,email;
    Activity activity;
    public FaceBookLoginApi(Activity activity,String id,String name,String email)
    {
        this.activity=activity;
        this.id=id;
        this.name=name;
        this.email=email;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Login.b_signIn.setVisibility(View.GONE);
        Login.progress_login.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("id",id)
                .add("name", name)
                .add("email", email)
                .add("medium", "phone")
                .build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String fblogin) {
        Log.d("value of fblogin", fblogin);
        try {

            JSONObject jsonObject=new JSONObject(fblogin);
            String result=jsonObject.getString("status");
            if (result.equals("success")) {
                JSONArray datajsonarray = jsonObject.getJSONArray("data");
                JSONObject userprofile=datajsonarray.getJSONObject(0);
                SplashScreen.user_mem_id = userprofile.getString("member_id");
                final SharedPreferences sharedPreferences = activity.getSharedPreferences("facebooklogin", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("fblogin", true);
                editor.putString("memberid_db", SplashScreen.user_mem_id);
                editor.commit();
                Intent intent = new Intent(activity, MainScreen.class);
                activity.startActivity(intent);
                activity.finish();
            } else if (result.equals("error")) {
                Login.b_signIn.setVisibility(View.VISIBLE);
                Login.progress_login.setVisibility(View.GONE);
                Toast.makeText(activity, "Invalid username or password", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Log.d("exception",e.toString());
            e.printStackTrace();
        }

    }
}