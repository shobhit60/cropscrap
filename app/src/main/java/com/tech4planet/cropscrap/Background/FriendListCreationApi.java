package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.tech4planet.cropscrap.Adapter.FriendListAdapter;
import com.tech4planet.cropscrap.Fragment.AddChequeFragment;
import com.tech4planet.cropscrap.Fragment.AddReminderRemindMeFragment;
import com.tech4planet.cropscrap.Fragment.FriendListFragment;
import com.tech4planet.cropscrap.Fragment.ReceiveBillFragment;
import com.tech4planet.cropscrap.R;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 30/11/17.
 */

public class FriendListCreationApi extends AsyncTask<String, Void, String> {

    Activity activity;
    ArrayList<String> friendname, friendid, friendmob;
    String fragment;

    public FriendListCreationApi(Activity activity, String fragment) {

        friendname = new ArrayList<>();
        friendid = new ArrayList<>();
        friendmob = new ArrayList<>();
        this.activity = activity;
        this.fragment = fragment;
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);

        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String s) {

        super.onPostExecute(s);
        if(s==null)
            Toast.makeText(activity,"No Internet Connection Available",Toast.LENGTH_LONG).show();
        else
        {
            try {
                JSONObject jsonObject = new JSONObject(s);
                //String result=jsonObject.getString("data");
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject myobject = jsonArray.getJSONObject(i);
                    friendid.add(myobject.getString("member_id"));
                    friendname.add(myobject.getString("name"));
                    friendmob.add(myobject.getString("phone"));
                }
                if (fragment.equals("friendlistfragment")) {
                    FriendListFragment.adapter = new FriendListAdapter(activity, friendname, friendmob, friendid);
                    FriendListFragment.recyclerview.setAdapter(FriendListFragment.adapter);
                }

                if (fragment.equals("addchequefragment")) {
                    AddChequeFragment.friendadapter = new ArrayAdapter(activity, android.R.layout.simple_list_item_1, friendname);
                    AddChequeFragment.friend_id.addAll(friendid);
                    AddChequeFragment.friend_spinner.setAdapter(AddChequeFragment.friendadapter);
                }
                if (fragment.equals("receivebillfragment")) {
                    ReceiveBillFragment.friendadapter = new ArrayAdapter(activity, android.R.layout.simple_list_item_1, friendname);
                    ReceiveBillFragment.friend_id.addAll(friendid);
                    ReceiveBillFragment.friend_spinner.setAdapter(ReceiveBillFragment.friendadapter);
                }
                if (fragment.equals("addreminderfragment")) {
                    AddReminderRemindMeFragment.friendadapter = new ArrayAdapter(activity, R.layout.spinner_group_item, friendname);
                    AddReminderRemindMeFragment.friendadapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
                    AddReminderRemindMeFragment.friend_id.addAll(friendid);
                    AddReminderRemindMeFragment.friend_spinner.setAdapter(AddReminderRemindMeFragment.friendadapter);
                }

                Log.d("value of s", s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("value of s", s);
        }



    }
}

