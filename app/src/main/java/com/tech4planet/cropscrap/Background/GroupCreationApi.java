package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 14/11/17.
 */

public class GroupCreationApi extends AsyncTask<String, Void, String> {
    String group_name, member_name, mobile, digi, remind, dutch, ebill;
    Activity activity;
    ArrayList<ContactModel> contactdetail;

    public GroupCreationApi(Activity activity, String group_name, String digi, String remind, String dutch, String ebill, ArrayList<ContactModel> contactdetail) {
        this.group_name = group_name;
        this.digi = digi;
        this.remind = remind;
        this.dutch = dutch;
        this.ebill = ebill;
        this.activity = activity;
        this.contactdetail = contactdetail;
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("group_name", group_name)
                .addFormDataPart("add_to_00", digi)
                .addFormDataPart("add_to_01", remind)
                .addFormDataPart("add_to_02", dutch)
                .addFormDataPart("add_to_03", ebill);
              /*  .addFormDataPart("member_name[0]","hitech")
                .addFormDataPart("member_name[1]","suitech")
                .addFormDataPart("member_mobile[0]","345")
                .addFormDataPart("member_mobile[1]","346");*/

        for (int i = 0; i < contactdetail.size(); i++) {
            multipartBuilder.addFormDataPart("member_name[" + i + "]", contactdetail.get(i).getName());
        }

        for (int i = 0; i < contactdetail.size(); i++) {
            multipartBuilder.addFormDataPart("member_mobile[" + i + "]", contactdetail.get(i).getNumber());
        }


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String s) {

        super.onPostExecute(s);

        if (s==null)
        {
            Toast.makeText(activity,"No Internet Connection Available",Toast.LENGTH_LONG).show();
        }
        else
        Log.d("value of s", s);

    }
}

