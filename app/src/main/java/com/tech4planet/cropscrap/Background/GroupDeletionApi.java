package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 29/11/17.
 */

public class GroupDeletionApi extends AsyncTask<String, Void, String> {

    Activity activity;
    String groupid;

    public GroupDeletionApi(Activity activity, String groupid) {

        this.activity = activity;
        this.groupid = groupid;
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("group_id", groupid);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String groupdelete) {

        super.onPostExecute(groupdelete);

        /*try {
            JSONObject jsonObject=new JSONObject(s);
            //String result=jsonObject.getString("data");
            JSONArray jsonArray=jsonObject.getJSONArray("data");
            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject myobject=jsonArray.getJSONObject(i);
                grouplist.add(myobject.getString("group_name"));
                groupid.add(myobject.getString("group_id"));
            }
            GroupListFragment.adapter=new GroupListAdapter(activity,grouplist);
            GroupListFragment.recyclerview.setAdapter(GroupListFragment.adapter);
            Log.d("value of s",s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/
        Log.d("value of groupdelete", groupdelete);
       /* activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });*/


    }
}

