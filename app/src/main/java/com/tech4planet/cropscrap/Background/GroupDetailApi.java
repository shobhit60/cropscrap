package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.tech4planet.cropscrap.Adapter.GroupListDetailAdapter;
import com.tech4planet.cropscrap.Activity.SplashScreen;
import com.tech4planet.cropscrap.Adapter.SharedBillGroupListEvenlyDetailAdapter;
import com.tech4planet.cropscrap.Adapter.SharedBillGroupListUnEvenlyDetailAdapter;
import com.tech4planet.cropscrap.Fragment.DutchIt_SharedBill_Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 16/12/17.
 */

public class GroupDetailApi extends AsyncTask<String, Void, String> {

    Activity activity;
    String groupid;
    ArrayList<String> mem_name, mem_mob;
    RecyclerView grp_detail_recyclerview;
    RecyclerView.Adapter adapter;
    String fragment;


    public GroupDetailApi(Activity activity,String groupid, String fragment,RecyclerView grp_detail_recyclerview,RecyclerView.Adapter adapter) {
        this.activity = activity;
        this.fragment=fragment;
        this.groupid = groupid;
        this.grp_detail_recyclerview=grp_detail_recyclerview;
        this.adapter=adapter;
        mem_name = new ArrayList<>();
        mem_mob = new ArrayList<>();
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("group_id", groupid);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String s) {

        super.onPostExecute(s);

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject myobject = jsonArray.getJSONObject(i);
                mem_name.add(myobject.getString("name"));
                mem_mob.add(myobject.getString("phone"));
            }
            DutchIt_SharedBill_Fragment.member_list_size=mem_name.size();
            if(fragment.equals("grp_list_fragment"))
            {
                adapter = new GroupListDetailAdapter(activity, mem_name, mem_mob);
                grp_detail_recyclerview.setAdapter(adapter);
            }
            else if(fragment.equals("shared_bill_fragment_with_evenly"))
            {
                adapter=new SharedBillGroupListEvenlyDetailAdapter(activity,mem_name);
                grp_detail_recyclerview.setAdapter(adapter);
            }
            else
            {
                adapter=new SharedBillGroupListUnEvenlyDetailAdapter(activity,mem_name);
                grp_detail_recyclerview.setAdapter(adapter);
            }

            Log.d("value of s", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("value of s", s);
       /* activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });*/


    }
}

