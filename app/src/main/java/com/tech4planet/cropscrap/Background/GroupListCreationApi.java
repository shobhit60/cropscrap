package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.tech4planet.cropscrap.Adapter.GroupListAdapter;
import com.tech4planet.cropscrap.Adapter.SharedBillGroupListAdapter;
import com.tech4planet.cropscrap.Fragment.AddChequeFragment;
import com.tech4planet.cropscrap.Fragment.AddReminderRemindMeFragment;
import com.tech4planet.cropscrap.Fragment.DutchIt_SharedBill_Fragment;
import com.tech4planet.cropscrap.Fragment.GroupListFragment;
import com.tech4planet.cropscrap.Fragment.ReceiveBillFragment;
import com.tech4planet.cropscrap.R;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 27/11/17.
 */

public class GroupListCreationApi extends AsyncTask<String, Void, String> {

    Activity activity;
    ArrayList<String> grouplist, groupid;
    String fragment;

    public GroupListCreationApi(Activity activity, String fragment) {

        grouplist = new ArrayList<>();
        groupid = new ArrayList<>();
        this.activity = activity;
        this.fragment = fragment;
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);

        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String groupstring) {

        super.onPostExecute(groupstring);
        if (groupstring==null)
        {
            Toast.makeText(activity,"No Internet Connection Available",Toast.LENGTH_LONG).show();
            Log.d("value of grouplist", "no value");
        }

        else {
            Log.d("value of grouplist", groupstring);
            try {
                JSONObject jsonObject = new JSONObject(groupstring);
                //String result=jsonObject.getString("data");
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject myobject = jsonArray.getJSONObject(i);
                    grouplist.add(myobject.getString("group_name"));
                    groupid.add(myobject.getString("group_id"));
                }
                if(fragment.equals("sharedbillfragment"))
                {
                    DutchIt_SharedBill_Fragment.sharedadapter = new SharedBillGroupListAdapter(activity, grouplist,groupid);
                    DutchIt_SharedBill_Fragment.sharedrecyclerview.setAdapter(DutchIt_SharedBill_Fragment.sharedadapter);
                }
                if (fragment.equals("grouplistfragment")) {
                    GroupListFragment.adapter = new GroupListAdapter(activity, grouplist, groupid);
                    GroupListFragment.recyclerview.setAdapter(GroupListFragment.adapter);
                }
                if (fragment.equals("addchequefragment")) {
                    AddChequeFragment.groupadapter = new ArrayAdapter(activity, android.R.layout.simple_list_item_1, grouplist);
                    AddChequeFragment.group_id.addAll(groupid);
                    AddChequeFragment.group_spinner.setAdapter(AddChequeFragment.groupadapter);
                }
                if (fragment.equals("receivebillfragment")) {
                    ReceiveBillFragment.groupadapter = new ArrayAdapter(activity, android.R.layout.simple_list_item_1, grouplist);
                    ReceiveBillFragment.group_id.addAll(groupid);
                    ReceiveBillFragment.group_spinner.setAdapter(ReceiveBillFragment.groupadapter);
                }
                if (fragment.equals("addreminderfragment")) {
                    AddReminderRemindMeFragment.groupadapter = new ArrayAdapter(activity, R.layout.spinner_group_item, grouplist);
                    AddReminderRemindMeFragment.groupadapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
                    AddReminderRemindMeFragment.group_id.addAll(groupid);
                    AddReminderRemindMeFragment.group_spinner.setAdapter(AddReminderRemindMeFragment.groupadapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }
}

