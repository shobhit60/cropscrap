package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 11/12/17.
 */

public class IssueBillApi extends AsyncTask<String, Void, String> {
    String name, email, mobile;
    Activity activity;
    ProgressDialog progressDialog;
    File file;
    public IssueBillApi(Activity activity, File file, String name, String email, String mobile) {
        this.activity = activity;
        this.file = file;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        progressDialog = new ProgressDialog(activity);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("attach_bill", file.getName(), RequestBody.create(MediaType.parse("image/*"),file))
                .addFormDataPart("customer_name", name)
                .addFormDataPart("customer_email_id", email)
                .addFormDataPart("customer_mobile_no", mobile);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String issuebill) {

        super.onPostExecute(issuebill);
        Log.d("value of issuebill", issuebill);
        if (issuebill==null)
        {
            Log.d("value of issuebill", "no value coming from server");
            progressDialog.cancel();
        }
        else
        {
            String status = null;
            Log.d("value of issuebill", issuebill);
            try {
                JSONObject jsonObject = new JSONObject(issuebill);
                status = jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (status.equals("success")) {
                progressDialog.cancel();
                Toast.makeText(activity, "Bill Added Succcessfully", Toast.LENGTH_LONG).show();
            } else {
                progressDialog.cancel();
                Toast.makeText(activity, "Network Error.Please Try After Some Time", Toast.LENGTH_LONG).show();
            }
        }


    }
}

