package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by shobhit on 02-Jan-18.
 */

public class ProfileApi extends AsyncTask<String, Void, String> {
    Activity activity;
    public ProfileApi(Activity activity) {
        this.activity = activity;

    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("member_id", SplashScreen.user_mem_id);
        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPostExecute(String profilestring) {

        super.onPostExecute(profilestring);
        if (profilestring==null)
        {
            Toast.makeText(activity,"No Internet Connection Available",Toast.LENGTH_LONG).show();
        }
        else
        {
            Log.d("value of profilestring",profilestring);
            try {
                JSONObject jsonObject = new JSONObject(profilestring);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                final JSONObject myobject = jsonArray.getJSONObject(0);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MainScreen.name=myobject.getString("name");
                                MainScreen.email=myobject.getString("email");
                                MainScreen.contact=myobject.getString("phone");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }
}