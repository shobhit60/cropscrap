package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 11/12/17.
 */

public class ReceiveBillApi extends AsyncTask<String, Void, String> {
    String seller_name_text, category_text, bill_date_text, shareradiogroup_text, friend_id_text, group_id_text;
    Activity activity;
    ProgressDialog progressDialog;
    File file;
    public ReceiveBillApi(Activity activity, String seller_name_text, String category_text, File file, String bill_date_text, String shareradiogroup_text, String friend_id_text, String group_id_text) {
        this.activity = activity;
        this.seller_name_text = seller_name_text;
        this.category_text = category_text;
        this.file = file;
        this.bill_date_text = bill_date_text;
        this.shareradiogroup_text = shareradiogroup_text;
        this.friend_id_text = friend_id_text;
        this.group_id_text = group_id_text;
        progressDialog = new ProgressDialog(activity);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id)
                .addFormDataPart("seller_name", seller_name_text)
                .addFormDataPart("category", category_text)
                .addFormDataPart("bill_date", bill_date_text)
                .addFormDataPart("remark", "party related money")
                .addFormDataPart("attach_bill", file.getName(), RequestBody.create(MediaType.parse("image"),file))
                .addFormDataPart("cheque_notification", "none")
                .addFormDataPart("action[0]", shareradiogroup_text)
                .addFormDataPart("friend_id", friend_id_text)
                .addFormDataPart("group_id", group_id_text);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String receivebill) {

        super.onPostExecute(receivebill);
        Log.d("receivebill", receivebill);
        if (receivebill==null)
        {
            Log.d("value of receivebill", "no value coming from server");
            progressDialog.cancel();
        }
        else
        {
            String status=null;
            Log.d("receivebill", receivebill);
            try {
                JSONObject jsonObject=new JSONObject(receivebill);
                status=jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(status.equals("success"))
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Bill Received Succcessfully",Toast.LENGTH_LONG).show();
            }
            else
            {
                progressDialog.cancel();
                Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
            }



        }
    }
}

