package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Activity.Login;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 8/11/17.
 */

public class RegistrationApi extends AsyncTask<String, Void, String> {
    String response, username, useremail, usermobile, password;
    Activity activity;

    public RegistrationApi(Activity activity, String username, String useremail, String usermobile, String password) {
        this.useremail = useremail;
        this.usermobile = usermobile;
        this.username = username;
        this.password = password;
        this.activity = activity;
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("name", username)
                .add("email", useremail)
                .add("phone", usermobile)
                .add("password", password)
                .add("medium","phone")
                .build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String s) {

        super.onPostExecute(s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            String result = jsonObject.getString("status");
            if (result.equals("success")) {  /*  final SharedPreferences sharedPreferences = Login.this.getSharedPreferences("app_settings", Login.this.MODE_PRIVATE);
                SharedPreferences.Editor editor =sharedPreferences.edit();
                editor.putBoolean("login",true);
                editor.putString("username",userid);
                editor.putString("password",password);
                editor.commit();
                okhttp.username=userid;
                okhttp.password=password;*/

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Login.progress_login.setVisibility(View.GONE);
                        Intent intent = new Intent(activity, MainScreen.class);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                });
            } else if (result.equals("error")) {   //b_signup.setVisibility(View.VISIBLE);
                //SignUp.progress_signup.setVisibility(View.GONE);
                if (Login.googlechange) {
                    Login.progress_login.setVisibility(View.GONE);
                    Intent intent = new Intent(activity, MainScreen.class);
                    activity.startActivity(intent);
                    activity.finish();
                }
                else
                    Toast.makeText(activity, "Email Already Exist", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

           /* Toast.makeText(RegistrationApi.this, s.toString(), Toast.LENGTH_SHORT).show();
            if (s.equals("SUCCESS")) {
                final SharedPreferences sharedPreferences = RegistrationApi.this.getSharedPreferences("app_settings", RegistrationApi.this.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("login", true);
                editor.putString("username",useremail);
                editor.putString("password",password);
                editor.commit();
                okhttp.username=useremail;
                okhttp.password=password;
                Intent intent = new Intent(RegistrationApi.this, MainScreen.class);
                startActivity(intent);
                finish();
            } else {
                progress_signup.setVisibility(View.INVISIBLE);
                b_signup.setVisibility(View.VISIBLE);
            }*/
        Log.d("value of s", s);

    }
}

