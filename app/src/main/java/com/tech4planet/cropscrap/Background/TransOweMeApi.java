package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.tech4planet.cropscrap.Activity.SplashScreen;
import com.tech4planet.cropscrap.Adapter.ViewChequeAdapter;
import com.tech4planet.cropscrap.Fragment.ViewChequeFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 8/12/17.
 */

public class TransOweMeApi extends AsyncTask<String, Void, String> {
    Activity activity;
    ArrayList<String> cheque_id,issuer_bank_name, cheque_no, issued_by, in_favour_of, amount, issued_date,shared_with,remark,check_file;

    public TransOweMeApi(Activity activity) {
        this.activity = activity;
       /* cheque_id=new ArrayList<>();
        issuer_bank_name = new ArrayList<>();
        cheque_no = new ArrayList<>();
        issued_by = new ArrayList<>();
        in_favour_of = new ArrayList<>();
        amount = new ArrayList<>();
        issued_date = new ArrayList<>();
        shared_with=new ArrayList<>();
        remark=new ArrayList<>();
        check_file=new ArrayList<>();*/
    }


    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();

        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    @Override
    protected void onPostExecute(String oweme) {

        super.onPostExecute(oweme);

        if(oweme==null)
        {
            Log.d("oweme","null value coming from server");
        }
        else
        {
            Log.d("oweme",oweme);
        }
       /* try {
            JSONObject jsonObject = new JSONObject(viewcheque);
            //String result=jsonObject.getString("data");
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject myobject = jsonArray.getJSONObject(i);
                cheque_id.add(myobject.getString("id").toUpperCase());
                issuer_bank_name.add(myobject.getString("issuer_bank_name").toUpperCase());
                cheque_no.add(myobject.getString("cheque_no").toUpperCase());

                issued_by.add(myobject.getString("issued_by").toUpperCase());
                in_favour_of.add(myobject.getString("in_favour_of").toUpperCase());

                amount.add(myobject.getString("amount").toUpperCase());
                issued_date.add(myobject.getString("issued_date").toUpperCase());
                remark.add(myobject.getString("remark").toUpperCase());
                check_file.add(myobject.getString("check_file"));
            }
            ViewChequeFragment.adapter = new ViewChequeAdapter(activity,cheque_id, issued_date, in_favour_of, amount, issuer_bank_name,cheque_no,issued_by,remark,check_file);
            ViewChequeFragment.view_cheque_recyclerview.setAdapter(ViewChequeFragment.adapter);


            Log.d("value of viewcheque", viewcheque);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }
}