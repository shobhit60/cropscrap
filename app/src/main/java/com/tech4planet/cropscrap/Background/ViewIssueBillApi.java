package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.tech4planet.cropscrap.Adapter.ViewIssueBillAdapter;
import com.tech4planet.cropscrap.Fragment.ViewIssueBillFragment;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 11/12/17.
 */

public class ViewIssueBillApi extends AsyncTask<String, Void, String> {
    ArrayList<String> id, attach_bill, customer_name, customer_email_id, customer_mobile_no;
    Activity activity;
    ProgressDialog progressDialog;

    public ViewIssueBillApi(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        id = new ArrayList<>();
        attach_bill = new ArrayList<>();
        customer_name = new ArrayList<>();
        customer_email_id = new ArrayList<>();
        customer_mobile_no = new ArrayList<>();
    }

/*
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }*/

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String viewbill) {

        super.onPostExecute(viewbill);
        try {
            JSONObject jsonObject = new JSONObject(viewbill);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject myobject = jsonArray.getJSONObject(i);
                id.add(myobject.getString("id").toUpperCase());
                attach_bill.add(myobject.getString("attach_bill"));

                customer_name.add(myobject.getString("customer_name").toUpperCase());
                customer_email_id.add(myobject.getString("customer_email_id").toUpperCase());

                customer_mobile_no.add(myobject.getString("customer_mobile_no").toUpperCase());

            }
            ViewIssueBillFragment.adapter = new ViewIssueBillAdapter(activity, id, attach_bill, customer_name, customer_email_id, customer_mobile_no);
            ViewIssueBillFragment.view_issue_bill_recyclerview.setAdapter(ViewIssueBillFragment.adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* String status=null;
        try {
            JSONObject jsonObject=new JSONObject(s);
            status=jsonObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(status.equals("success"))
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Bill Added Succcessfully",Toast.LENGTH_LONG).show();
        }
        else
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
        }*/
        Log.d("value of viewbill", viewbill);
    }
}

