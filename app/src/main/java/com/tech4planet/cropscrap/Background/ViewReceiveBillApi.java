package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.tech4planet.cropscrap.Adapter.ViewReceiveBillAdapter;
import com.tech4planet.cropscrap.Fragment.ViewReceiveBillFragment;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 12/12/17.
 */

public class ViewReceiveBillApi extends AsyncTask<String, Void, String> {
    ArrayList<String> receivebill_id,seller_name, category, date ,remark,attach_bill;
    Activity activity;
    ProgressDialog progressDialog;

    public ViewReceiveBillApi(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        receivebill_id=new ArrayList<>();
        seller_name = new ArrayList<>();
        category = new ArrayList<>();
        date = new ArrayList<>();
        remark=new ArrayList<>();
        attach_bill=new ArrayList<>();
    }

/*
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }*/

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String viewreceivebill) {

        super.onPostExecute(viewreceivebill);

        if (viewreceivebill.isEmpty()) {
            Log.d("viewreceivebill-value", "no response is getting from server");
        } else {
            try {
                Log.d("viewreceivebill-value", viewreceivebill);
                JSONObject jsonObject = new JSONObject(viewreceivebill);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject myobject = jsonArray.getJSONObject(i);
                    receivebill_id.add(myobject.getString("id").toUpperCase());
                    seller_name.add(myobject.getString("seller_name").toUpperCase());
                    category.add(myobject.getString("category").toUpperCase());
                    date.add(myobject.getString("date").toUpperCase());
                    remark.add(myobject.getString("remark").toUpperCase());
                    attach_bill.add(myobject.getString("attach_bill"));
                }
                ViewReceiveBillFragment.adapter = new ViewReceiveBillAdapter(activity,receivebill_id, seller_name, category, date, remark,attach_bill);
                ViewReceiveBillFragment.view_receive_bill_recyclerview.setAdapter(ViewReceiveBillFragment.adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

       /* String status=null;
        try {
            JSONObject jsonObject=new JSONObject(s);
            status=jsonObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(status.equals("success"))
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Bill Added Succcessfully",Toast.LENGTH_LONG).show();
        }
        else
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
        }*/

    }
}

