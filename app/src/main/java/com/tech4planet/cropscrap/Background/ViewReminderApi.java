package com.tech4planet.cropscrap.Background;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.tech4planet.cropscrap.Adapter.ViewEventRemindMeAdapter;
import com.tech4planet.cropscrap.Fragment.ViewEventRemindMeFragment;
import com.tech4planet.cropscrap.Activity.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tech4planet on 15/12/17.
 */

public class ViewReminderApi extends AsyncTask<String, Void, String> {
    ArrayList<String> reminder_id, reminder_purpose,reminder_notification, reminder_from_date, reminder_to_date,reminder_remarks,shared_with,reminder_file;
    Activity activity;
    ProgressDialog progressDialog;

    public ViewReminderApi(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        reminder_id=new ArrayList<>();
        reminder_purpose = new ArrayList<>();
        reminder_notification=new ArrayList<>();
        reminder_from_date = new ArrayList<>();
        reminder_to_date = new ArrayList<>();
        reminder_remarks=new ArrayList<>();
        shared_with=new ArrayList<>();
        reminder_file=new ArrayList<>();
    }

/*
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setMessage("Wait for a minute,Dont go back or cancel");
        progressDialog.incrementProgressBy(0);
        progressDialog.incrementSecondaryProgressBy(0);
        progressDialog.show();
    }*/

    @Override
    protected String doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("medium", "phone")
                .addFormDataPart("creator_id", SplashScreen.user_mem_id);


        RequestBody formBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(params[0])
                .post(formBody)
                .build();
        try (Response response = client.newCall(request).execute()) {

            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String viewremindme) {

        super.onPostExecute(viewremindme);

        if (viewremindme.isEmpty()) {
            Log.d("viewremindme-value", "no response is getting from server");
        } else {
            Log.d("viewremindme-value", viewremindme);
            try {

                JSONObject jsonObject = new JSONObject(viewremindme);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject myobject = jsonArray.getJSONObject(i);
                    reminder_id.add(myobject.getString("reminder_id").toUpperCase());
                    reminder_purpose.add(myobject.getString("reminder_purpose").toUpperCase());
                    reminder_notification.add(myobject.getString("reminder_notification").toUpperCase());
                    reminder_from_date.add(myobject.getString("reminder_from_date").toUpperCase());
                    reminder_to_date.add(myobject.getString("reminder_to_date").toUpperCase());
                    reminder_remarks.add(myobject.getString("reminder_remarks").toUpperCase());
                   // shared_with.add(myobject.getString("shared_with").toUpperCase());
                    reminder_file.add(myobject.getString("reminder_file"));
                }
                ViewEventRemindMeFragment.adapter = new ViewEventRemindMeAdapter(activity,reminder_id, reminder_purpose,reminder_notification, reminder_from_date, reminder_to_date,reminder_remarks,shared_with,reminder_file);
                ViewEventRemindMeFragment.view_event_recyclerview.setAdapter(ViewEventRemindMeFragment.adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

       /* String status=null;
        try {
            JSONObject jsonObject=new JSONObject(s);
            status=jsonObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(status.equals("success"))
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Bill Added Succcessfully",Toast.LENGTH_LONG).show();
        }
        else
        {
            progressDialog.cancel();
            Toast.makeText(activity,"Network Error.Please Try After Some Time",Toast.LENGTH_LONG).show();
        }*/

    }
}

