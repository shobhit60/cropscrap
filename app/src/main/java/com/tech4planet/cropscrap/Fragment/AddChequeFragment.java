package com.tech4planet.cropscrap.Fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.tech4planet.cropscrap.Activity.FriendActivity;
import com.tech4planet.cropscrap.Activity.GroupActivity;
import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Background.AddChequeApi;
import com.tech4planet.cropscrap.Background.FriendListCreationApi;
import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.Background.ProfileApi;
import com.tech4planet.cropscrap.Background.ViewChequeApi;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.tech4planet.cropscrap.R.id.imageView;


public class AddChequeFragment extends Fragment {

    RadioGroup share_rg, radiogroup;
    Spinner bank_spinner;
    EditText cheque_no, issued_by, in_favour_of, amount;
    TextView issueddate,upload_file;
    RelativeLayout select_friend_rl, select_group_rl;
    Button form_submit_btn, friend_submit_btn,add_friend_btn, group_submit_btn,add_group_btn;
    ArrayAdapter bankadapter;

    public static Spinner friend_spinner, group_spinner;
    public static ArrayAdapter friendadapter, groupadapter;
    public static ArrayList<String> friend_id, group_id;

    View view;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    File file;
    String friend_id_text, group_id_text;
    final String BANK[] = new String[]{
            "Axis Bank Ltd",
            "State Bank of India",
            "HDFC Bank Ltd",
            "Punjab National Bank",
            "CITI Bank",
            "Bank of Baroda",
            "Allahabad Bank",
            "Andhra Bank",
            "Bank of India",
            "Canara Bank",
            "Central Bank of India",
            "Corporation Bank",
            "ICICI Bank",
            "IDBI Bank",
            "Indian Bank",
            "Indian Overseas Bank",
            "IndusInd Bank",
            "Kotak Bank",
            "Oriental Bank of Commerce",
            "State Bank of Bikaner & Jaipur",
            "State Bank of Hyderabad",
            "State Bank of Mysore",
            "State Bank of Patiala",
            "State Bank of Travancore",
            "Syndicate Bank",
            "State Bank of Mysore",
            "UCO Bank",
            "Union Bank of India",
            "Vijaya Bank",
            "Bank of Maharashtra",
            "Federal Bank",
            "ING Vysya Bank",
            "Karur Vysya Bank",
            "Jammu and Kashmir Bank",
            "Karnataka Bank Ltd",
            "Laxmi-Vilas-Bank",
            "South Indian Bank",
            "Syndicate Bank",
    };

    public AddChequeFragment() {
        friend_id = new ArrayList<>();
        group_id = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_cheque_fragment, container, false);
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        radiogroup = (RadioGroup) view.findViewById(R.id.radiogroup);
        share_rg = (RadioGroup) view.findViewById(R.id.share_rg);

        bank_spinner = (Spinner) view.findViewById(R.id.bank_spinner);
        friend_spinner = (Spinner) view.findViewById(R.id.friend_spinner);
        group_spinner = (Spinner) view.findViewById(R.id.group_spinner);

        cheque_no = (EditText) view.findViewById(R.id.cheque_no);
        issued_by = (EditText) view.findViewById(R.id.issued_by);
        in_favour_of = (EditText) view.findViewById(R.id.in_favour_of);
        amount = (EditText) view.findViewById(R.id.amount);

        issueddate = (TextView) view.findViewById(R.id.issueddate);
        upload_file=(TextView)view.findViewById(R.id.choose_file);


        select_friend_rl = (RelativeLayout) view.findViewById(R.id.select_friend_rl);
        select_group_rl = (RelativeLayout) view.findViewById(R.id.select_group_rl);

        form_submit_btn = (Button) view.findViewById(R.id.form_submit_btn);
        friend_submit_btn = (Button) view.findViewById(R.id.friend_submit_btn);
        add_friend_btn=(Button)view.findViewById(R.id.add_friend_btn);
        group_submit_btn = (Button) view.findViewById(R.id.group_submit_btn);
        add_group_btn=(Button)view.findViewById(R.id.add_group_btn);


        bankadapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, BANK);
        bank_spinner.setAdapter(bankadapter);



        share_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.none:
                        select_friend_rl.setVisibility(View.GONE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.VISIBLE);
                        break;
                    case R.id.friend:
                        select_friend_rl.setVisibility(View.VISIBLE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                    case R.id.group:
                        select_group_rl.setVisibility(View.VISIBLE);
                        select_friend_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                }
            }
        });

        friend_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                friend_id_text = friend_id.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                group_id_text = group_id.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        issueddate.setText(sdf.format(myCalendar.getTime()));
                    }

                };
        issueddate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(getActivity(), R.style.CalenderHeaderColor, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;

            }
        });

        upload_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        form_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        friend_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_friend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FriendActivity.class));
                getActivity().finish();

            }
        });
        group_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_group_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GroupActivity.class));
                getActivity().finish();
            }
        });



        new FriendListCreationApi(getActivity(), "addchequefragment").execute("http://13.126.131.147/friend-list/");
        new GroupListCreationApi(getActivity(), "addchequefragment").execute("http://13.126.131.147/group-list");
        return view;
    }


    public void submit_detail() {
        String cheque_no_text=cheque_no.getText().toString();
        String issuer_name_text=issued_by.getText().toString();
        String receiver_name_text=in_favour_of.getText().toString();
        String amt_text=amount.getText().toString();
        String issueddate_text=issueddate.getText().toString();
        String upload_file_text=upload_file.getText().toString();
        String bank_spinner_text=bank_spinner.getSelectedItem().toString();
        networkInfo = connMgr.getActiveNetworkInfo();
        String shareradiogroup_text=((RadioButton)view.findViewById(share_rg.getCheckedRadioButtonId())).getText().toString();
        if(shareradiogroup_text.equals("None"))
        {
            group_id_text="";
            friend_id_text="";
        }
        else if(shareradiogroup_text.equals("Friend"))
        {
            group_id_text="";
        }
        else if(shareradiogroup_text.equals("Group"))
        {
            friend_id_text="";
        }
        if(networkInfo == null)
            showSettingsAlert();
       else if(TextUtils.isEmpty(cheque_no_text))
        {
            cheque_no.setFocusable(true);
            cheque_no.setError("Enter Cheque No");
        }
        else if(TextUtils.isEmpty(issuer_name_text))
        {
            issued_by.setFocusable(true);
            issued_by.setError("Enter Issuer Name");
        }
        else if(TextUtils.isEmpty(receiver_name_text))
        {
            in_favour_of.setFocusable(true);
            in_favour_of.setError("Enter Receiver's Name");
        }
        else if(TextUtils.isEmpty(amt_text))
        {
            amount.setFocusable(true);
            amount.setError("Enter Amount");
        }
        else if(TextUtils.isEmpty(issueddate_text))
        {
            issueddate.setFocusable(true);
            issueddate.setError("Select Date");
        }
        else if(TextUtils.isEmpty(upload_file_text))
        {
            upload_file.setFocusable(true);
            upload_file.setError("Choose File");
        }
        else
        {
            new AddChequeApi(getActivity(),bank_spinner_text,cheque_no_text,issuer_name_text,receiver_name_text,amt_text,issueddate_text,file,shareradiogroup_text,friend_id_text,group_id_text).execute("http://13.126.131.147/admin/digi_cheque_insert");
            new ViewChequeApi(getActivity()).execute("http://13.126.131.147/view-cheque");
            cheque_no.setText("");
            issued_by.setText("");
            in_favour_of.setText("");
            amount.setText("");
            issueddate.setText("");
            upload_file.setText("");
        }

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            if (null != filePath) {
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                String filename = file.getName();
                upload_file.setText(filename);
            }
        }
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
