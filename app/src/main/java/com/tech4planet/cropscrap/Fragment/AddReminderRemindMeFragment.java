package com.tech4planet.cropscrap.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.tech4planet.cropscrap.Activity.FriendActivity;
import com.tech4planet.cropscrap.Activity.GroupActivity;
import com.tech4planet.cropscrap.Background.AddReminderApi;
import com.tech4planet.cropscrap.Background.FriendListCreationApi;
import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.Background.ViewReminderApi;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.tech4planet.cropscrap.R.id.single_day_rg;


public class AddReminderRemindMeFragment extends Fragment {

    public static Spinner friend_spinner, group_spinner;
    public static ArrayAdapter friendadapter, groupadapter;
    public static ArrayList<String> friend_id, group_id;
    public static TextView recurring_select, fromcalender, tocalender;
    Spinner set_alert_spinner, recurring_spinner;
    ArrayAdapter<CharSequence> alertadapter, recurringadapter;
    RadioGroup share_rg, recurring_rg, reminder_rg, single_day_rg;
    RelativeLayout select_friend_rl, select_group_rl;
    Button form_submit_btn, friend_submit_btn,add_friend_btn, group_submit_btn,add_group_btn;
    EditText remindme, remarks;
    String friend_id_text, group_id_text, set_alertid_text, recurring_id_text;
    TextView choose_file, location, datecalender,start_time,end_time;
    AddReminderCalender addReminderCalender;
    View view;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    File file;

    ConnectivityManager connMgr;
    NetworkInfo networkInfo;

    LinearLayout calenderlayout,anothercalenderlayout;
    RelativeLayout singledaycalenderrl,calenderrl,anothercalenderrl;
    View from_calender_left_view,from_calender_down_view, to_calender_left_view, to_calender_down_view;
    public AddReminderRemindMeFragment() {
        // Required empty public constructor
        friend_id = new ArrayList<>();
        group_id = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.add_reminder_remindme_fragment, container, false);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        remindme = (EditText) view.findViewById(R.id.remindme);
        location = (TextView) view.findViewById(R.id.location);
        remarks = (EditText) view.findViewById(R.id.remarks);

        set_alert_spinner = (Spinner) view.findViewById(R.id.set_alert);
        friend_spinner = (Spinner) view.findViewById(R.id.friend_spinner);
        group_spinner = (Spinner) view.findViewById(R.id.group_spinner);
        alertadapter = ArrayAdapter.createFromResource(getActivity(), R.array.alert_spinner, R.layout.spinner_group_item);
        alertadapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
        set_alert_spinner.setAdapter(alertadapter);
        select_friend_rl = (RelativeLayout) view.findViewById(R.id.select_friend_rl);
        select_group_rl = (RelativeLayout) view.findViewById(R.id.select_group_rl);
        form_submit_btn = (Button) view.findViewById(R.id.form_submit_btn);
        friend_submit_btn = (Button) view.findViewById(R.id.friend_submit_btn);
        add_friend_btn=(Button)view.findViewById(R.id.add_friend_btn);
        group_submit_btn = (Button) view.findViewById(R.id.group_submit_btn);
        add_group_btn=(Button)view.findViewById(R.id.add_group_btn);
        share_rg = (RadioGroup) view.findViewById(R.id.share_rg);
        share_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.none:
                        select_friend_rl.setVisibility(View.GONE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.VISIBLE);
                        break;
                    case R.id.friend:
                        select_friend_rl.setVisibility(View.VISIBLE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                    case R.id.group:
                        select_group_rl.setVisibility(View.VISIBLE);
                        select_friend_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                }
            }
        });
        recurring_select = (TextView) view.findViewById(R.id.recurring_select);
        recurring_spinner = (Spinner) view.findViewById(R.id.recurring_spinner);
        recurringadapter = ArrayAdapter.createFromResource(getActivity(), R.array.recurring_spinner, R.layout.spinner_group_item);
        recurringadapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
        recurring_spinner.setAdapter(recurringadapter);
        recurring_rg = (RadioGroup) view.findViewById(R.id.recurring_rg);
        recurring_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {

                    case R.id.no:
                        recurring_select.setVisibility(View.GONE);
                        recurring_spinner.setVisibility(View.GONE);
                        break;
                    case R.id.yes:
                        recurring_select.setVisibility(View.VISIBLE);
                        recurring_spinner.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

         final RelativeLayout singledaycalenderrl,calenderrl,anothercalenderrl;
        singledaycalenderrl=(RelativeLayout)view.findViewById(R.id.singledaycalenderrl);
        calenderrl=(RelativeLayout)view.findViewById(R.id.calenderrl);
        anothercalenderrl=(RelativeLayout)view.findViewById(R.id.anothercalenderrl);
        reminder_rg=(RadioGroup)view.findViewById(R.id.reminder_rg);
        single_day_rg=(RadioGroup)view.findViewById(R.id.single_day_rg);
        reminder_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId)
                {
                    case R.id.single_day:
                        int radioButtonID = single_day_rg.getCheckedRadioButtonId();
                        RadioButton rb=(RadioButton)single_day_rg.findViewById(radioButtonID);
                        Log.d("rb text",rb.getText().toString());
                        if(rb.getText().toString().trim().equals("Select Date"))
                            singledaycalenderrl.setVisibility(View.VISIBLE);
                        else
                            singledaycalenderrl.setVisibility(View.GONE);
                        single_day_rg.setVisibility(View.VISIBLE);
                        calenderrl.setVisibility(View.GONE);
                        anothercalenderrl.setVisibility(View.GONE);
                        break;
                    case R.id.multi_day:
                        single_day_rg.setVisibility(View.GONE);
                        singledaycalenderrl.setVisibility(View.GONE);
                        calenderrl.setVisibility(View.VISIBLE);
                        anothercalenderrl.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        single_day_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId)
                {
                    case R.id.today:
                        singledaycalenderrl.setVisibility(View.GONE);
                        calenderrl.setVisibility(View.GONE);
                        anothercalenderrl.setVisibility(View.GONE);
                        break;
                    case R.id.tomorrow:
                        singledaycalenderrl.setVisibility(View.GONE);
                        calenderrl.setVisibility(View.GONE);
                        anothercalenderrl.setVisibility(View.GONE);
                        break;
                    case R.id.select_date:
                        singledaycalenderrl.setVisibility(View.VISIBLE);
                        calenderrl.setVisibility(View.GONE);
                        anothercalenderrl.setVisibility(View.GONE);
                        break;
                }
            }
        });


        choose_file = (TextView) view.findViewById(R.id.choose_file);
        fromcalender = (TextView) view.findViewById(R.id.fromcalender);
        tocalender = (TextView) view.findViewById(R.id.tocalender);
        fromcalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReminderCalender.Add_Reminder_Calender(R.id.fromcalender);

            }
        });

        tocalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReminderCalender.Add_Reminder_Calender(R.id.tocalender);
            }
        });

        friend_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                friend_id_text = friend_id.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                group_id_text = group_id.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        set_alert_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set_alertid_text = set_alert_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        recurring_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recurring_id_text = recurring_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {

                } catch (GooglePlayServicesNotAvailableException e) {

                }

            }
        });

        datecalender=(TextView)view.findViewById(R.id.datecalender);
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        datecalender.setText(sdf.format(myCalendar.getTime()));

                    }

                };

        datecalender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(getActivity(), R.style.CalenderHeaderColor, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });
        start_time=(TextView)view.findViewById(R.id.start_time);
        end_time=(TextView)view.findViewById(R.id.end_time);
        start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        start_time.setText(hourOfDay + ":" + minute);
                    }


                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        end_time.setText(hourOfDay + ":" + minute);
                    }


                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        choose_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        form_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        friend_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_friend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FriendActivity.class));
                getActivity().finish();

            }
        });
        group_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_group_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GroupActivity.class));
                getActivity().finish();
            }
        });
        new FriendListCreationApi(getActivity(), "addreminderfragment").execute("http://13.126.131.147/friend-list/");
        new GroupListCreationApi(getActivity(), "addreminderfragment").execute("http://13.126.131.147/group-list");
        return view;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


            filePath = data.getData();

            if (null != filePath) {
                // Get the path from the Uri
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                String filename = file.getName();
                String fileabsolutepath=file.getAbsolutePath();
                Log.d("fileabsolutepath",fileabsolutepath);
                choose_file.setText(filename);

            }
        }
        else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                location.setText(place.getName());
                Log.i("PlaceName", "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.i("PlaceError", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }


    public void submit_detail() {
        String remindme_text, location_text, fromcalender_text, tocalender_text, datecalender_text, remarks_text,start_time_text,end_time_text;
        String recurring_rg_text, share_rg_text, reminder_rg_text,single_day_rg_text="";
        remindme_text = remindme.getText().toString();
        location_text = location.getText().toString();
        fromcalender_text = fromcalender.getText().toString();
        tocalender_text = tocalender.getText().toString();
        datecalender_text=datecalender.getText().toString();
        remarks_text = remarks.getText().toString();
        networkInfo = connMgr.getActiveNetworkInfo();
        start_time_text=start_time.getText().toString();
        end_time_text=end_time.getText().toString();
        reminder_rg_text=((RadioButton) view.findViewById(reminder_rg.getCheckedRadioButtonId())).getText().toString();
        if(reminder_rg_text.equals("Multi Day"))
        {
            single_day_rg_text=datecalender_text="";
            if(TextUtils.isEmpty(fromcalender_text))
            {
                fromcalender.setFocusable(true);
                fromcalender.setError("Enter Date");
            }
            else if(TextUtils.isEmpty(tocalender_text))
            {
                tocalender.setFocusable(true);
                tocalender.setError("Enter Date");
            }

        }
        else{
            fromcalender_text=tocalender_text="";
            single_day_rg_text=((RadioButton) view.findViewById(single_day_rg.getCheckedRadioButtonId())).getText().toString();
            switch (single_day_rg_text)
            {
                case "Today":datecalender_text="";
                    break;
                case "Tomorrow":datecalender_text="";
                    break;
                case "Select Date":
                    if(TextUtils.isEmpty(datecalender_text))
                    {
                        datecalender.setFocusable(true);
                        datecalender.setError("Enter Date");
                    }
                    break;
            }
        }

        recurring_rg_text = ((RadioButton) view.findViewById(recurring_rg.getCheckedRadioButtonId())).getText().toString();
        if (recurring_rg_text.equals("No")) {
            recurring_id_text = "";
        }
        share_rg_text = ((RadioButton) view.findViewById(share_rg.getCheckedRadioButtonId())).getText().toString();
        if (share_rg_text.equals("None")) {
            group_id_text = "";
            friend_id_text = "";
        } else if (share_rg_text.equals("Friend")) {
            group_id_text = "";
        } else if (share_rg_text.equals("Group")) {
            friend_id_text = "";
        }

        if(networkInfo == null)
            showSettingsAlert();
       else if (TextUtils.isEmpty(remindme_text)) {
            remindme.setFocusable(true);
            remindme.setError("Enter RemindMe");
        }
        else if (TextUtils.isEmpty(location_text)) {
            location.setFocusable(true);
            location.setError("Enter Location");
        }
        else if(TextUtils.isEmpty(start_time_text))
        {
            start_time.setFocusable(true);
            start_time.setError("Enter Start Time");
        }
        else if(TextUtils.isEmpty(end_time_text))
        {
            end_time.setFocusable(true);
            end_time.setError("Enter End Time");
        }
        else if (TextUtils.isEmpty(remarks_text)) {
            remarks.setFocusable(true);
            remarks.setError("Enter Remarks");
        } else {
            new AddReminderApi(getActivity(), remindme_text, location_text, reminder_rg_text,single_day_rg_text, fromcalender_text, tocalender_text,datecalender_text,start_time_text,end_time_text,file,recurring_rg_text, recurring_id_text, set_alertid_text, remarks_text, share_rg_text, friend_id_text, group_id_text).execute("http://13.126.131.147/admin/reminder_insert/");
            new ViewReminderApi(getActivity()).execute("http://13.126.131.147/view-event");
            /*remindme.setText("");
            location.setText("");
            fromcalender.setText("");
            tocalender.setText("");
            remarks.setText("");
            choose_file.setText("");*/
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        addReminderCalender = (AddReminderCalender) activity;
    }

    public interface AddReminderCalender {
        void Add_Reminder_Calender(int id);
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                dialog.cancel();
            }
        });

        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }
}
