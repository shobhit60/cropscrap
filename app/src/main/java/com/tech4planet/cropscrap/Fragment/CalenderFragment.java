package com.tech4planet.cropscrap.Fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.tech4planet.cropscrap.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalenderFragment extends Fragment {

    DatePicker datepicker;
    Button set_date_btn;

    SelectSDate selectSDate;
    public CalenderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.calender_fragment, container, false);
        datepicker = (DatePicker) v.findViewById(R.id.datepicker);
        set_date_btn = (Button) v.findViewById(R.id.set_date_btn);

        set_date_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* TimerFragment.datetextview.setText(datepicker.getMonth() + 1 + "-" + datepicker.getDayOfMonth() + "-" + datepicker.getYear());
                CalenderDialogFragment.viewPager.setCurrentItem(1);
                Log.d("value of day", "hello");*/
                selectSDate.Select_Date(""+datepicker.getYear(),""+datepicker.getMonth() + 1,""+datepicker.getDayOfMonth());
            }
        });

        return v;
    }

    public interface SelectSDate
    {
        public void Select_Date(String month,String day,String year);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        selectSDate=(SelectSDate)activity;
    }
}
