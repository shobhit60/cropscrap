package com.tech4planet.cropscrap.Fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.tech4planet.cropscrap.Background.Dutchit_Debt_Api;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.tech4planet.cropscrap.R.id.recurring_rg;

public class DutchIt_Debt_Fragment extends Fragment {

    RadioGroup select_dept_rg;
    TextInputLayout gave_other_textinputlayout, gave_me_textinputlayout;
    Spinner currency_spinner;
    TextView issueddate,choose_file;
    EditText purpose,amount,note,gave_to_other_editext,gave_me_editext;
    Button form_submit_btn;
    LinearLayout calenderlayout;


    ArrayAdapter currency_adapter;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    File file;
    String currencytext;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dutchit_debt_fragment, container, false);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        purpose=(EditText)view.findViewById(R.id.purpose);
        amount=(EditText)view.findViewById(R.id.amount);
        gave_to_other_editext=(EditText)view.findViewById(R.id.gave_to_other_editext);
        gave_me_editext=(EditText)view.findViewById(R.id.gave_me_editext);
        note=(EditText)view.findViewById(R.id.note);
        gave_other_textinputlayout = (TextInputLayout) view.findViewById(R.id.gave_other_textinputlayout);
        gave_me_textinputlayout = (TextInputLayout) view.findViewById(R.id.gave_me_textinputlayout);
        select_dept_rg = (RadioGroup) view.findViewById(R.id.select_dept_rg);
        currency_spinner = (Spinner) view.findViewById(R.id.currency_spinner);
        issueddate = (TextView) view.findViewById(R.id.issueddate);
        calenderlayout=(LinearLayout)view.findViewById(R.id.calenderlayout);

        gave_other_textinputlayout.setVisibility(View.VISIBLE);
        select_dept_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.gave_other:
                        gave_other_textinputlayout.setVisibility(View.VISIBLE);
                        gave_me_textinputlayout.setVisibility(View.GONE);
                        break;
                    case R.id.gave_me:
                        gave_me_textinputlayout.setVisibility(View.VISIBLE);
                        gave_other_textinputlayout.setVisibility(View.GONE);
                        break;
                }
            }
        });

        currency_adapter = ArrayAdapter.createFromResource(getActivity(), R.array.currency_spinner, R.layout.spinner_group_item);
        currency_adapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
        currency_spinner.setAdapter(currency_adapter);


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        issueddate.setText(sdf.format(myCalendar.getTime()));
                    }

                };
        issueddate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(getActivity(), R.style.CalenderHeaderColor, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });

        choose_file=(TextView)view.findViewById(R.id.choose_file);
        choose_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        currency_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currencytext = currency_spinner.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        form_submit_btn=(Button)view.findViewById(R.id.form_submit_btn);
        form_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String purposestring,amountstring,notestring,owestext;
                String select_dept_rg_text,issued_date_text,choose_file_text;
                purposestring=purpose.getText().toString();
                amountstring=amount.getText().toString();
                notestring=note.getText().toString();
                issued_date_text=issueddate.getText().toString();
                choose_file_text=choose_file.getText().toString();
                networkInfo = connMgr.getActiveNetworkInfo();

                select_dept_rg_text = ((RadioButton) view.findViewById(select_dept_rg.getCheckedRadioButtonId())).getText().toString();
                if (select_dept_rg_text.equals("I gave someone")) {
                    select_dept_rg_text = "i-gave";
                    owestext=gave_to_other_editext.getText().toString();
                    Log.d("gave someone owetext",owestext);
                }
                else
                {
                 select_dept_rg_text= "someone-gave";
                    owestext=gave_me_editext.getText().toString();
                    Log.d("gave me owetext",owestext);
                }

                if(networkInfo == null)
                    showSettingsAlert();
               else  if(TextUtils.isEmpty(purposestring))
                {
                    purpose.setFocusable(true);
                    purpose.setError("Enter Purpose");

                }
                else if(TextUtils.isEmpty(issued_date_text))
                {
                    issueddate.setFocusable(true);
                    issueddate.setError("Select Date");

                }
                else if(TextUtils.isEmpty(amountstring))
                {
                    amount.setFocusable(true);
                    amount.setError("Select Amount");

                }
                else if(TextUtils.isEmpty(choose_file_text))
                {
                    choose_file.setFocusable(true);
                    choose_file.setError("Select File");
                }
                else
                {
                    new Dutchit_Debt_Api(getActivity(),select_dept_rg_text,owestext,purposestring,issued_date_text,amountstring,currencytext,notestring,file).execute("http://13.126.131.147/admin/add_debt_insert");
                    gave_to_other_editext.setText("");
                    gave_me_editext.setText("");
                    purpose.setText("");
                    issueddate.setText("");
                    amount.setText("");
                    note.setText("");
                    choose_file.setText("");
                }
            }
        });

        return view;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            if (null != filePath) {
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                String filename = file.getName();
                choose_file.setText(filename);
            }
        }
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
