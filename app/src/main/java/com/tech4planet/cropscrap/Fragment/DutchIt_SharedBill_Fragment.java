package com.tech4planet.cropscrap.Fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class DutchIt_SharedBill_Fragment extends Fragment {

    EditText purpose,note,email_editext_id,amt_editext_id,unevenly_email_editext_id,unevenly_amt_editext_id,unevenly_share_editext_id;
    TextView issueddate;
    Spinner currency_spinner;
    Button add_btn,unevenly_add_btn,form_submit_btn;
    LinearLayout evenly_email_container,unevenly_email_container,evenly_with_email,unevenly_with_email,grp_list_ll;
    ArrayAdapter currency_adapter;

    public static LinearLayout grp_dtl_list_ll,grp_dtl_list_unevenly_ll;
    public static RadioGroup share_rg,share_evenly_rg;

    public static RecyclerView sharedrecyclerview;
    public static RecyclerView.Adapter sharedadapter;
    RecyclerView.LayoutManager sharedlayoutManager;
    public static RecyclerView shareddetailevenlyrecyclerview;
    public static RecyclerView.Adapter sharedetailevenlydadapter;
    RecyclerView.LayoutManager shareddetailevenlylayoutManager;
    public static RecyclerView shareddetailunevenlyrecyclerview;
    public static RecyclerView.Adapter sharedetailunevenlydadapter;
    RecyclerView.LayoutManager shareddetailunevelylayoutManager;

    List<View> addview,unevenlyaddview;
    int i=0;
    public static  int member_list_size;
    public static String mem_amt[],mem_share[];

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_shared_bill, container, false);
        purpose=(EditText)view.findViewById(R.id.purpose);
        note=(EditText)view.findViewById(R.id.note);
        email_editext_id=(EditText)view.findViewById(R.id.email_editext_id);
        amt_editext_id=(EditText)view.findViewById(R.id.amt_editext_id);
        unevenly_email_editext_id=(EditText)view.findViewById(R.id.unevenly_email_editext_id);
        unevenly_amt_editext_id=(EditText)view.findViewById(R.id.unevenly_amt_editext_id);
        unevenly_share_editext_id=(EditText)view.findViewById(R.id.unevenly_share_editext_id);
        issueddate = (TextView) view.findViewById(R.id.issueddate);
        currency_spinner = (Spinner) view.findViewById(R.id.currency_spinner);
        form_submit_btn=(Button) view.findViewById(R.id.form_submit_btn);
        add_btn=(Button)view.findViewById(R.id.add_btn);
        unevenly_add_btn=(Button)view.findViewById(R.id.unevenly_add_btn);
        share_rg=(RadioGroup)view.findViewById(R.id.share_rg);
        share_evenly_rg=(RadioGroup)view.findViewById(R.id.share_evenly_rg);
        evenly_with_email=(LinearLayout)view.findViewById(R.id.evenly_with_email);
        unevenly_with_email=(LinearLayout)view.findViewById(R.id.unevenly_with_email);
        evenly_email_container=(LinearLayout)view.findViewById(R.id.evenly_email_container);
        unevenly_email_container=(LinearLayout)view.findViewById(R.id.unevenly_email_container);
        grp_list_ll=(LinearLayout)view.findViewById(R.id.grp_list_ll);
        grp_dtl_list_ll=(LinearLayout)view.findViewById(R.id.grp_dtl_list_ll);
        grp_dtl_list_unevenly_ll=(LinearLayout)view.findViewById(R.id.grp_dtl_list_unevenly_ll);
        addview=new ArrayList<View>();
        unevenlyaddview=new ArrayList<View>();


        currency_adapter = ArrayAdapter.createFromResource(getActivity(), R.array.currency_spinner, R.layout.spinner_group_item);
        currency_adapter.setDropDownViewResource(R.layout.spinner_group_item_dropdown);
        currency_spinner.setAdapter(currency_adapter);

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        issueddate.setText(sdf.format(myCalendar.getTime()));
                    }

                };
        issueddate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(getActivity(), R.style.CalenderHeaderColor, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;

            }
        });


        share_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int radioButtonID = share_evenly_rg.getCheckedRadioButtonId();
                RadioButton rb=(RadioButton)share_evenly_rg.findViewById(radioButtonID);
                switch (checkedId)
                {
                    case R.id.evenly:
                        unevenly_email_container.removeAllViews();
                        grp_dtl_list_ll.setVisibility(View.GONE);
                        shareddetailevenlyrecyclerview.setVisibility(View.GONE);
                        grp_dtl_list_unevenly_ll.setVisibility(View.GONE);
                        if(rb.getText().toString().trim().equals("Share With Email"))
                        {
                            evenly_with_email.setVisibility(View.VISIBLE);
                            unevenly_with_email.setVisibility(View.GONE);
                            sharedrecyclerview.setVisibility(View.GONE);
                            grp_list_ll.setVisibility(View.GONE);


                        }
                        else
                        {
                            evenly_with_email.setVisibility(View.GONE);
                            unevenly_with_email.setVisibility(View.GONE);
                            sharedrecyclerview.setVisibility(View.VISIBLE);
                            grp_list_ll.setVisibility(View.VISIBLE);
                        }
                        break;
                    case R.id.unevenly:
                        evenly_email_container.removeAllViews();
                        grp_dtl_list_ll.setVisibility(View.GONE);
                        shareddetailevenlyrecyclerview.setVisibility(View.GONE);
                        grp_dtl_list_unevenly_ll.setVisibility(View.GONE);
                        if(rb.getText().toString().trim().equals("Share With Email"))
                        {
                            evenly_with_email.setVisibility(View.GONE);
                            unevenly_with_email.setVisibility(View.VISIBLE);
                            sharedrecyclerview.setVisibility(View.GONE);
                        }
                        else
                        {
                            evenly_with_email.setVisibility(View.GONE);
                            unevenly_with_email.setVisibility(View.GONE);
                            sharedrecyclerview.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });

        share_evenly_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int radioButtonID = share_rg.getCheckedRadioButtonId();
                RadioButton rb=(RadioButton)share_rg.findViewById(radioButtonID);
                switch (checkedId)
                {
                    case R.id.with_email:
                        sharedrecyclerview.setVisibility(View.GONE);
                        grp_list_ll.setVisibility(View.GONE);
                        grp_dtl_list_ll.setVisibility(View.GONE);
                        shareddetailevenlyrecyclerview.setVisibility(View.GONE);
                        grp_dtl_list_unevenly_ll.setVisibility(View.GONE);
                        unevenly_email_container.removeAllViews();
                        evenly_email_container.removeAllViews();
                        if(rb.getText().toString().trim().equals("Share Evenly"))
                        {
                            evenly_with_email.setVisibility(View.VISIBLE);
                            unevenly_with_email.setVisibility(View.GONE);
                        }
                        else
                        {
                            evenly_with_email.setVisibility(View.GONE);
                            unevenly_with_email.setVisibility(View.VISIBLE);
                        }
                        break;
                    case R.id.with_group:
                        evenly_with_email.setVisibility(View.GONE);
                        unevenly_with_email.setVisibility(View.GONE);
                        sharedrecyclerview.setVisibility(View.VISIBLE);
                        grp_list_ll.setVisibility(View.VISIBLE);
                        grp_dtl_list_ll.setVisibility(View.GONE);
                        shareddetailevenlyrecyclerview.setVisibility(View.GONE);
                        grp_dtl_list_unevenly_ll.setVisibility(View.GONE);
                        unevenly_email_container.removeAllViews();
                        evenly_email_container.removeAllViews();
                        break;
                }
            }
        });

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(email_editext_id.getText())||TextUtils.isEmpty(amt_editext_id.getText()))
                    Toast.makeText(getActivity(),"Please Enter email and amount",Toast.LENGTH_SHORT).show();
                else
                {
                    LayoutInflater layoutInflater =
                            (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.evenly_with_emaillayout, null);
                    addview.add(addView);
                    Button remove=(Button)addView.findViewById(R.id.remove_btn);
                    Log.d("size of addview",addview.size()+"");
                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addview.remove(addView);
                            Log.d("size of addview",addview.size()+"");
                            ((LinearLayout)addView.getParent()).removeView(addView);
                        }
                    });
                    evenly_email_container.addView(addView);
                }

            }
        });

        unevenly_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(unevenly_email_editext_id.getText())||TextUtils.isEmpty(unevenly_amt_editext_id.getText())||TextUtils.isEmpty(unevenly_share_editext_id.getText()))
                    Toast.makeText(getActivity(),"Please Enter email,amount and share",Toast.LENGTH_SHORT).show();
                else {
                    LayoutInflater layoutInflater =
                            (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View unevenlyaddView = layoutInflater.inflate(R.layout.unevenly_with_emaillayout, null);
                    unevenlyaddview.add(unevenlyaddView);
                    Button remove = (Button) unevenlyaddView.findViewById(R.id.remove_btn);
                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            unevenlyaddview.remove(unevenlyaddView);
                            ((LinearLayout) unevenlyaddView.getParent()).removeView(unevenlyaddView);
                        }
                    });
                    unevenly_email_container.addView(unevenlyaddView);
                }
            }
        });

        sharedrecyclerview = (RecyclerView) view.findViewById(R.id.shared_bill_recyclerview);
        sharedlayoutManager = new LinearLayoutManager(getActivity());
        sharedrecyclerview.setLayoutManager(sharedlayoutManager);
        sharedrecyclerview.setNestedScrollingEnabled(false);
        sharedrecyclerview.setHasFixedSize(true);

        shareddetailevenlyrecyclerview = (RecyclerView) view.findViewById(R.id.shared_dtl_bill_evenly_recyclerview);
        shareddetailevenlylayoutManager = new LinearLayoutManager(getActivity());
        shareddetailevenlyrecyclerview.setLayoutManager(shareddetailevenlylayoutManager);
        shareddetailevenlyrecyclerview.setNestedScrollingEnabled(false);
        shareddetailevenlyrecyclerview.setHasFixedSize(true);

        shareddetailunevenlyrecyclerview = (RecyclerView) view.findViewById(R.id.shared_dtl_bill_evenly_recyclerview);
        shareddetailunevelylayoutManager = new LinearLayoutManager(getActivity());
        shareddetailunevenlyrecyclerview.setLayoutManager(shareddetailunevelylayoutManager);
        shareddetailunevenlyrecyclerview.setNestedScrollingEnabled(false);
        shareddetailunevenlyrecyclerview.setHasFixedSize(true);
        new GroupListCreationApi(getActivity(), "sharedbillfragment").execute("http://13.126.131.147/group-list");

        form_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String purposestring=purpose.getText().toString();
                String datestring=issueddate.getText().toString();
                String notestring=note.getText().toString();
                int share_rg_ID = share_rg.getCheckedRadioButtonId();
                int share_evenly_rg_ID = share_evenly_rg.getCheckedRadioButtonId();

                RadioButton share_rg_text=(RadioButton)share_rg.findViewById(share_rg_ID);
                RadioButton share_evenly_rg_text=(RadioButton)share_evenly_rg.findViewById(share_evenly_rg_ID);
                Log.d("share_rg_text",share_rg_text.getText().toString());
                Log.d("share_evenly_rg_text",share_evenly_rg_text.getText().toString());
                if(TextUtils.isEmpty(purposestring))
                {
                    purpose.setError("Enter purpose");
                    purpose.setFocusable(true);
                }
                else if(TextUtils.isEmpty(datestring))
                {
                    issueddate.setError("Select Date");
                    issueddate.setFocusable(true);
                }
                else if(TextUtils.isEmpty(notestring))
                {
                    note.setError("Enter note");
                    note.setFocusable(true);
                }
                else if(share_rg_text.getText().toString().trim().equals("Share Evenly")&& share_evenly_rg_text.getText().toString().trim().equals("Share With Email"))
                {

                    String email=email_editext_id.getText().toString();
                    String amt=amt_editext_id.getText().toString();
                    if(TextUtils.isEmpty(email))
                        Toast.makeText(getActivity(),"Please Enter Email",Toast.LENGTH_SHORT).show();
                    else if(TextUtils.isEmpty(amt))
                        Toast.makeText(getActivity(),"Please Enter Amount",Toast.LENGTH_SHORT).show();
                    else if(addview.size()!=0)
                    {
                        ArrayList<String> email_arraylist=new ArrayList<String>();
                        ArrayList<String> amt_arraylist=new ArrayList<String>();
                        for(int i=0;i<addview.size();i++)
                        {
                            EditText email_editext_id=(EditText)addview.get(i).findViewById(R.id.email_editext_id);
                            EditText amt_editext_id=(EditText)addview.get(i).findViewById(R.id.amt_editext_id);
                            email_arraylist.add(email_editext_id.getText().toString());
                            amt_arraylist.add(amt_editext_id.getText().toString());
                        }
                    }
                }
                else if(share_rg_text.getText().toString().trim().equals("Share Unevenly")&& share_evenly_rg_text.getText().toString().trim().equals("Share With Email")) {
                    String email=unevenly_email_editext_id.getText().toString();
                    String amt=unevenly_amt_editext_id.getText().toString();
                    String share=unevenly_share_editext_id.getText().toString();
                    if(TextUtils.isEmpty(email))
                        Toast.makeText(getActivity(),"Please Enter Email",Toast.LENGTH_SHORT).show();
                    else if(TextUtils.isEmpty(amt))
                        Toast.makeText(getActivity(),"Please Enter Amount",Toast.LENGTH_SHORT).show();
                    else if(TextUtils.isEmpty(share))
                        Toast.makeText(getActivity(),"Please Enter Share",Toast.LENGTH_SHORT).show();
                    if(unevenlyaddview.size()!=0)
                    {
                        ArrayList<String> email_arraylist=new ArrayList<String>();
                        ArrayList<String> amt_arraylist=new ArrayList<String>();
                        ArrayList<String> share_arraylist=new ArrayList<String>();
                        for(int i=0;i<unevenlyaddview.size();i++)
                        {
                            EditText email_editext_id=(EditText)unevenlyaddview.get(i).findViewById(R.id.unevenly_email_editext_id);
                            EditText amt_editext_id=(EditText)unevenlyaddview.get(i).findViewById(R.id.unevenly_amt_editext_id);
                            EditText share_editext_id=(EditText)unevenlyaddview.get(i).findViewById(R.id.unevenly_share_editext_id);
                            email_arraylist.add(email_editext_id.getText().toString());
                            amt_arraylist.add(amt_editext_id.getText().toString());
                            share_arraylist.add(share_editext_id.getText().toString());
                        }
                    }
                }
                else if(share_rg_text.getText().toString().trim().equals("Share Evenly")&& share_evenly_rg_text.getText().toString().trim().equals("Share With Group")) {

                    for (int i = 0; i < mem_amt.length; i++) {
                        Log.d("mem_amt_value=",mem_amt[i]);
                    }
                }

                else if(share_rg_text.getText().toString().trim().equals("Share Unevenly")&& share_evenly_rg_text.getText().toString().trim().equals("Share With Group")) {

                    for (int i = 0; i < mem_share.length; i++) {
                        Log.d("mem_amt_value=",mem_amt[i]);
                        Log.d("mem_share_value=",mem_share[i]);
                    }
                }
            }
        });
        return view;
    }
}
