package com.tech4planet.cropscrap.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tech4planet.cropscrap.Activity.Transaction_History;
import com.tech4planet.cropscrap.Activity.Transaction_I_Owe;
import com.tech4planet.cropscrap.Activity.Transaction_Owes_Me;
import com.tech4planet.cropscrap.Activity.Transaction_Settled;
import com.tech4planet.cropscrap.R;

public class DutchIt_Transaction_Fragment extends Fragment {


    FrameLayout whoowsmecard,whomiowecard,settledtransactioncard,sharedcard;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dutchit_transaction_fragment, container, false);
        whoowsmecard=(FrameLayout)view.findViewById(R.id.whoowsmecard);
        whomiowecard=(FrameLayout)view.findViewById(R.id.whomiowecard);
        settledtransactioncard=(FrameLayout)view.findViewById(R.id.settledtransactioncard);
        sharedcard=(FrameLayout)view.findViewById(R.id.sharedcard);
        whoowsmecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            startActivity(new Intent(getActivity(), Transaction_Owes_Me.class));
            }
        });
        whomiowecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Transaction_I_Owe.class));
            }
        });
        settledtransactioncard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Transaction_Settled.class));
            }
        });
        sharedcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Transaction_History.class));
            }
        });
        return view;
    }
}
