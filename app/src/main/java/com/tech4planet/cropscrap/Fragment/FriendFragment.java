package com.tech4planet.cropscrap.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.FriendActivity;
import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Adapter.ContactFilterAdapter;
import com.tech4planet.cropscrap.Background.FriendCreationApi;
import com.tech4planet.cropscrap.Background.FriendListCreationApi;
import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendFragment extends Fragment {

    Button add_friend_btn, submitbutton;
    RecyclerView recyclerview;
    RecyclerView.LayoutManager layoutManager;
    public static RecyclerView.Adapter adapter;
    ArrayList<ContactModel> newlist;
    friend_creation g_c;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    public FriendFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.friend_fragment, container, false);
        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        add_friend_btn = (Button) v.findViewById(R.id.add_friend_btn);
        submitbutton = (Button) v.findViewById(R.id.submitbutton);
        recyclerview = (RecyclerView) v.findViewById(R.id.recyclerview);
        if (!MainScreen.list.isEmpty()) {
            newlist = new ArrayList<ContactModel>();
            layoutManager = new LinearLayoutManager(this.getActivity());
            recyclerview.setLayoutManager(layoutManager);
            recyclerview.setHasFixedSize(true);

            for (ContactModel model : MainScreen.list) {
                if (model.isSelected()) {
                    //model.setSelected(false);
                    newlist.add(model);
                }
            }

            adapter = new ContactFilterAdapter(this.getActivity(), newlist);
            recyclerview.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (!newlist.isEmpty())
                submitbutton.setVisibility(View.VISIBLE);
        }
        add_friend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g_c.friend_create(R.id.add_friend_btn);
            }
        });
        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else if (newlist.isEmpty()) {
                    submitbutton.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Please add member before proceding", Toast.LENGTH_LONG).show();
                }
                else {
                    new FriendCreationApi(getActivity(), newlist).execute("http://13.126.131.147/admin/add_friend_insert/");
                    new FriendListCreationApi(getActivity(), "friendlistfragment").execute("http://13.126.131.147/friend-list/");
                    Toast.makeText(getActivity(), "Friends Added Successfully", Toast.LENGTH_LONG).show();
                    submitbutton.setVisibility(View.GONE);
                    newlist.clear();
                    adapter.notifyDataSetChanged();


                }

            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        g_c = (friend_creation) activity;
    }

    public interface friend_creation {
        void friend_create(int id);
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                dialog.cancel();
            }
        });

        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }
}
