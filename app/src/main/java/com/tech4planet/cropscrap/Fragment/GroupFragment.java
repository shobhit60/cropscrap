package com.tech4planet.cropscrap.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Adapter.ContactFilterAdapter;
import com.tech4planet.cropscrap.Background.GroupCreationApi;
import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.Pojo.ContactModel;
import com.tech4planet.cropscrap.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {


    EditText grp_name_et;
    RelativeLayout add, close, add_mem_rl, submit_button_ll;
    CheckBox digi_cheque_cb, remind_me_cb, dutch_it_cb, e_bill_cb;
    group_creation g_c;
    Button add_mem_btn, submitbutton;
    RecyclerView recyclerview;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    ArrayList<ContactModel> newlist;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.group_fragment, container, false);
        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        //submit_button_ll=(RelativeLayout)view.findViewById(R.id.submit_button_ll);
        submitbutton = (Button) view.findViewById(R.id.submitbutton);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        grp_name_et = (EditText) view.findViewById(R.id.grp_name_et);
        digi_cheque_cb = (CheckBox) view.findViewById(R.id.digi_cheque_cb);
        remind_me_cb = (CheckBox) view.findViewById(R.id.remind_me_cb);
        dutch_it_cb = (CheckBox) view.findViewById(R.id.dutch_it_cb);
        e_bill_cb = (CheckBox) view.findViewById(R.id.e_bill_cb);
        if (!MainScreen.list.isEmpty()) {
            newlist = new ArrayList<ContactModel>();
            layoutManager = new LinearLayoutManager(this.getActivity());
            recyclerview.setLayoutManager(layoutManager);
            recyclerview.setHasFixedSize(true);
            recyclerview.setNestedScrollingEnabled(false);
            for (ContactModel model : MainScreen.list) {
                if (model.isSelected()) {
                    //model.setSelected(false);
                    newlist.add(model);
                }
            }
            adapter = new ContactFilterAdapter(this.getActivity(), newlist);
            recyclerview.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (!newlist.isEmpty())
                submitbutton.setVisibility(View.VISIBLE);
        }
        /*activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/


        add_mem_btn = (Button) view.findViewById(R.id.add_mem_btn);
        /*add_mem_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g_c.group_create(R.id.add_mem_rl);
            }
        });*/

        add_mem_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g_c.group_create(R.id.add_mem_btn);
            }
        });

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String group_name = grp_name_et.getText().toString();
                String e_bill, digi_cheque, remind_me, dutch_it;
                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();

                else if (TextUtils.isEmpty(group_name)) {
                     grp_name_et.setFocusable(true);
                    grp_name_et.setError("Enter group name");

                } else {

                    if (e_bill_cb.isChecked())
                        e_bill = "1";
                    else {
                        e_bill_cb.setChecked(true);
                        e_bill = "1";
                    }


                    if (digi_cheque_cb.isChecked())
                        digi_cheque = "1";
                    else {
                        digi_cheque_cb.setChecked(true);
                        digi_cheque = "1";
                    }

                    if (remind_me_cb.isChecked())
                        remind_me = "1";
                    else {
                        remind_me_cb.setChecked(true);
                        remind_me = "1";
                    }

                    if (dutch_it_cb.isChecked())
                        dutch_it = "1";
                    else {
                        dutch_it_cb.setChecked(true);
                        dutch_it = "1";
                    }

                    if (newlist.isEmpty()) {
                        submitbutton.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Please add member before proceding", Toast.LENGTH_LONG).show();
                    } else {

                        new GroupCreationApi(getActivity(), group_name, digi_cheque, remind_me, dutch_it, e_bill, newlist).execute("http://13.126.131.147/admin/insert_group/");
                        new GroupListCreationApi(getActivity(), "grouplistfragment").execute("http://13.126.131.147/group-list");
                        Toast.makeText(getActivity(), "Members Added Successfully", Toast.LENGTH_LONG).show();
                        grp_name_et.setText("");
                        submitbutton.setVisibility(View.GONE);
                        newlist.clear();
                        adapter.notifyDataSetChanged();

                    }

                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        g_c = (group_creation) activity;
    }

    public interface group_creation {
        void group_create(int id);
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                dialog.cancel();
            }
        });

        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }
}
