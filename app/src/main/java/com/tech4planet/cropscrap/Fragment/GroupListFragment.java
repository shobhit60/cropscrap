package com.tech4planet.cropscrap.Fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.Background.ViewChequeApi;
import com.tech4planet.cropscrap.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupListFragment extends Fragment {


    public static RecyclerView recyclerview;
    public static RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    public GroupListFragment() {
        // Required empty public constructor
        Log.d("inside constructor", "grouplistfragment");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.group_list_fragment, container, false);
        recyclerview = (RecyclerView) v.findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.setHasFixedSize(true);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GroupListCreationApi(getActivity(), "grouplistfragment").execute("http://13.126.131.147/group-list");
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo == null)
            showSettingsAlert();
        else
        new GroupListCreationApi(getActivity(), "grouplistfragment").execute("http://13.126.131.147/group-list");


       // setHasOptionsMenu(true);
        return v;
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else
                    new GroupListCreationApi(getActivity(), "grouplistfragment").execute("http://13.126.131.147/group-list");
            }
        });


        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }

  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new GroupListCreationApi(getActivity(), "grouplistfragment").execute("http://13.126.131.147/group-list");
                Toast.makeText(getActivity(), "New Data Loaded", Toast.LENGTH_LONG).show();
                break;

        }
        return true;

    }
*/
}
