package com.tech4planet.cropscrap.Fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tech4planet.cropscrap.Background.IssueBillApi;
import com.tech4planet.cropscrap.Background.ViewIssueBillApi;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;

import static android.app.Activity.RESULT_OK;


public class IssueBillFragment extends Fragment {
    TextView choose_file;
    EditText name_editext_id, email_editext_id, mobile_editext_id;
    Button submitbutton;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    File file;
    public IssueBillFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issue_bill, container, false);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        choose_file=(TextView) view.findViewById(R.id.choose_file);

        submitbutton = (Button) view.findViewById(R.id.submitbutton);

        name_editext_id = (EditText) view.findViewById(R.id.name_editext_id);
        email_editext_id = (EditText) view.findViewById(R.id.email_editext_id);
        mobile_editext_id = (EditText) view.findViewById(R.id.mobile_editext_id);

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bill = choose_file.getText().toString();
                String name = name_editext_id.getText().toString();
                String email = email_editext_id.getText().toString();
                String mobile = mobile_editext_id.getText().toString();
                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else if (TextUtils.isEmpty(bill)) {
                    choose_file.setError("Enter Bill");
                    choose_file.setFocusable(true);
                } else if (TextUtils.isEmpty(name)) {
                    name_editext_id.setError("Enter Name");
                    name_editext_id.setFocusable(true);
                } else if (TextUtils.isEmpty(email)) {
                    email_editext_id.setError("Enter Email");
                    email_editext_id.setFocusable(true);
                } else if (TextUtils.isEmpty(mobile)) {
                    mobile_editext_id.setError("Enter Mobile");
                    mobile_editext_id.setFocusable(true);
                } else {
                    new IssueBillApi(getActivity(), file, name, email, mobile).execute("http://13.126.131.147/admin/insert_issue_bills/");
                    new ViewIssueBillApi(getActivity()).execute("http://13.126.131.147/view-issue-bills");
                    name_editext_id.setText("");
                    email_editext_id.setText("");
                    mobile_editext_id.setText("");
                    choose_file.setText("");
                }
            }
        });
        choose_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        return view;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            if (null != filePath) {
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                String filename = file.getName();
                choose_file.setText(filename);
            }
        }
    }
    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
