package com.tech4planet.cropscrap.Fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tech4planet.cropscrap.Activity.FriendActivity;
import com.tech4planet.cropscrap.Activity.GroupActivity;
import com.tech4planet.cropscrap.Background.FriendListCreationApi;
import com.tech4planet.cropscrap.Background.GroupListCreationApi;
import com.tech4planet.cropscrap.Background.ReceiveBillApi;
import com.tech4planet.cropscrap.Background.ViewReceiveBillApi;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class ReceiveBillFragment extends Fragment {
    RelativeLayout select_friend_rl, select_group_rl;
    RadioGroup share_rg;
    Button form_submit_btn, friend_submit_btn,add_friend_btn, group_submit_btn,add_group_btn;
    EditText seller_name, category;
    TextView issueddate,choose_file;
    public static Spinner friend_spinner, group_spinner;
    public static ArrayAdapter friendadapter, groupadapter;
    public static ArrayList<String> friend_id, group_id;

    String friend_id_text, group_id_text;
    View view;
    private Uri filePath;
    File file;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    private int PICK_IMAGE_REQUEST = 1;

    public ReceiveBillFragment() {
        friend_id = new ArrayList<>();
        group_id = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_receive_bill, container, false);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        issueddate = (TextView) view.findViewById(R.id.issueddate);
        choose_file=(TextView)view.findViewById(R.id.choose_file);
        seller_name = (EditText) view.findViewById(R.id.seller_name);
        category = (EditText) view.findViewById(R.id.category);
        friend_spinner = (Spinner) view.findViewById(R.id.friend_spinner);
        group_spinner = (Spinner) view.findViewById(R.id.group_spinner);
        form_submit_btn = (Button) view.findViewById(R.id.form_submit_btn);
        friend_submit_btn = (Button) view.findViewById(R.id.friend_submit_btn);
        add_friend_btn=(Button)view.findViewById(R.id.add_friend_btn);
        group_submit_btn = (Button) view.findViewById(R.id.group_submit_btn);
        add_group_btn=(Button)view.findViewById(R.id.add_group_btn);
        select_friend_rl = (RelativeLayout) view.findViewById(R.id.select_friend_rl);
        select_group_rl = (RelativeLayout) view.findViewById(R.id.select_group_rl);
        share_rg = (RadioGroup) view.findViewById(R.id.share_rg);
        share_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.none:
                        select_friend_rl.setVisibility(View.GONE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.VISIBLE);
                        break;
                    case R.id.friend:
                        select_friend_rl.setVisibility(View.VISIBLE);
                        select_group_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                    case R.id.group:
                        select_group_rl.setVisibility(View.VISIBLE);
                        select_friend_rl.setVisibility(View.GONE);
                        form_submit_btn.setVisibility(View.GONE);
                        break;
                }
            }
        });

        friend_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                friend_id_text = friend_id.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });


        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                group_id_text = group_id.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        issueddate.setText(sdf.format(myCalendar.getTime()));
                    }

                };
        issueddate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(getActivity(), R.style.CalenderHeaderColor, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });

        choose_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        form_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        friend_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_friend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FriendActivity.class));
                getActivity().finish();

            }
        });
        group_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_detail();
            }
        });
        add_group_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GroupActivity.class));
                getActivity().finish();
            }
        });
        new FriendListCreationApi(getActivity(), "receivebillfragment").execute("http://13.126.131.147/friend-list/");
        new GroupListCreationApi(getActivity(), "receivebillfragment").execute("http://13.126.131.147/group-list");
        return view;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            if (null != filePath) {
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                String filename = file.getName();
                choose_file.setText(filename);
            }
        }
    }

    public void submit_detail() {
        String seller_name_text, category_text, attach_bill_text, bill_date_text;
        String shareradiogroup_text;
        seller_name_text = seller_name.getText().toString();
        category_text = category.getText().toString();
        attach_bill_text = choose_file.getText().toString();
        bill_date_text = issueddate.getText().toString();
        networkInfo = connMgr.getActiveNetworkInfo();
        shareradiogroup_text = ((RadioButton) view.findViewById(share_rg.getCheckedRadioButtonId())).getText().toString();
        if (shareradiogroup_text.equals("None")) {
            group_id_text = "";
            friend_id_text = "";
        } else if (shareradiogroup_text.equals("Friend")) {
            group_id_text = "";
        } else if (shareradiogroup_text.equals("Group")) {
            friend_id_text = "";
        }

        if(networkInfo == null)
            showSettingsAlert();
        else if (TextUtils.isEmpty(seller_name_text)) {
            seller_name.setFocusable(true);
            seller_name.setError("Enter Seller Name");
        } else if (TextUtils.isEmpty(category_text)) {
            category.setFocusable(true);
            category.setError("Enter Category");
        }
        else if (TextUtils.isEmpty(bill_date_text)) {
            issueddate.setFocusable(true);
            issueddate.setError("Select Date");
        }
        else if (TextUtils.isEmpty(attach_bill_text)) {
            choose_file.setFocusable(true);
            choose_file.setError("Upload File");
        } else
        {
            new ReceiveBillApi(getActivity(), seller_name_text, category_text, file, bill_date_text, shareradiogroup_text, friend_id_text, group_id_text).execute("http://13.126.131.147/admin/received_bills_insert/");
            new ViewReceiveBillApi(getActivity()).execute("http://13.126.131.147/view-bills/");
            seller_name.setText("");
            category.setText("");
            choose_file.setText("");
            issueddate.setText("");
        }

    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
