package com.tech4planet.cropscrap.Fragment;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.tech4planet.cropscrap.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimerFragment extends Fragment {

    public static TextView datetextview;
    TimePicker simpleTimePicker;
    int hour, min;
    Button ok_btn, cancel_btn;
    SetTimer setTimer;

    public TimerFragment() {
        // Required empty public constructor


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.timer_fragment, container, false);
        simpleTimePicker = (TimePicker) v.findViewById(R.id.simpleTimePicker);
        simpleTimePicker.setIs24HourView(true);
        hour = simpleTimePicker.getHour();
        min = simpleTimePicker.getMinute();

        datetextview = (TextView) v.findViewById(R.id.datetextview);
        ok_btn = (Button) v.findViewById(R.id.ok_btn);
        cancel_btn = (Button) v.findViewById(R.id.cancel_btn);
        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour = hourOfDay;
                min = minute;
            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimer.set_timer(R.id.ok_btn, datetextview.getText().toString() + " " + hour + ":" + min);
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimer.set_timer(R.id.cancel_btn, "");
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setTimer = (SetTimer) activity;
    }

    public interface SetTimer {
        void set_timer(int id, String timer);
    }
}
