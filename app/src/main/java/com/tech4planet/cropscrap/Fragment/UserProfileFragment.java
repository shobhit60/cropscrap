package com.tech4planet.cropscrap.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.tech4planet.cropscrap.Activity.MainScreen;
import com.tech4planet.cropscrap.Background.ProfileApi;
import com.tech4planet.cropscrap.OtherFiles.ImageFilePath;
import com.tech4planet.cropscrap.R;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    CardView upload_image_cardview;
    EditText up_username,up_email,up_contact;
    ImageView upload_image;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    File file;
    ProgressDialog progressDialog;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.user_profile_fragment, container, false);
        // Inflate the layout for this fragment
        up_username=(EditText)view.findViewById(R.id.up_username);
        up_email=(EditText)view.findViewById(R.id.up_email);
        up_contact=(EditText)view.findViewById(R.id.up_contact);
        upload_image_cardview=(CardView)view.findViewById(R.id.upload_image_cardview);
        upload_image=(ImageView)view.findViewById(R.id.upload_image);


        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo == null)
            showSettingsAlert();
        else
        {
            up_username.setText(MainScreen.name);
            up_email.setText(MainScreen.email);
            up_contact.setText(MainScreen.contact);
        }

        upload_image_cardview.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        return view;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
         if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


            filePath = data.getData();
            Bitmap bitmap=null;
            if (null != filePath) {
                // Get the path from the Uri
                String path = ImageFilePath.getPath(getActivity(), filePath);
                file=new File(path);
                try {
                     bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                upload_image.setImageBitmap(bitmap);
            }
        }
    }


    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

               networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else
                    new ProfileApi(getActivity()).execute("http://13.126.131.147/profile/");
            }
        });


        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }

}
