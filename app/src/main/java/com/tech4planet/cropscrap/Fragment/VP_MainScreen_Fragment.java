package com.tech4planet.cropscrap.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tech4planet.cropscrap.Activity.DigiChequeActivity;
import com.tech4planet.cropscrap.Activity.DutchItActivity;
import com.tech4planet.cropscrap.Activity.EbillActivity;
import com.tech4planet.cropscrap.Activity.RemindMeActivity;
import com.tech4planet.cropscrap.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VP_MainScreen_Fragment extends Fragment {

    private FrameLayout fl_digi_cheque, fl_remind_me, fl_dutch_it, fl_ebill;

    public VP_MainScreen_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.vp_main_screen_fragment_layout, container, false);
        fl_digi_cheque = (FrameLayout) view.findViewById(R.id.fl_digi_cheque);
        View.OnClickListener digi_cheque_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DigiChequeActivity.class);
                startActivity(intent);
            }
        };
        fl_digi_cheque.setOnClickListener(digi_cheque_click_listener);

        fl_remind_me = (FrameLayout) view.findViewById(R.id.fl_remind_me);
        View.OnClickListener remind_me_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RemindMeActivity.class);
                startActivity(intent);
            }
        };
        fl_remind_me.setOnClickListener(remind_me_click_listener);

        fl_dutch_it = (FrameLayout) view.findViewById(R.id.fl_dutch_it);
        View.OnClickListener dutch_it_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DutchItActivity.class);
                startActivity(intent);
            }
        };
        fl_dutch_it.setOnClickListener(dutch_it_click_listener);

        fl_ebill = (FrameLayout) view.findViewById(R.id.fl_ebill);
        View.OnClickListener ebill_click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EbillActivity.class));
                //Log.d("activity","clicked");
            }
        };
        fl_ebill.setOnClickListener(ebill_click_listener);
        return view;
    }

}
