package com.tech4planet.cropscrap.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech4planet.cropscrap.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VendorProfileFragment extends Fragment {


    public VendorProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vendorprofilefragment, container, false);
    }

}
