package com.tech4planet.cropscrap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tech4planet.cropscrap.Background.ProfileApi;
import com.tech4planet.cropscrap.Background.ViewChequeApi;
import com.tech4planet.cropscrap.R;


public class ViewChequeFragment extends Fragment {

    public static RecyclerView view_cheque_recyclerview;
    public static RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_cheque_fragment, container, false);

        //code for recyclerview
        view_cheque_recyclerview = (RecyclerView) view.findViewById(R.id.view_cheque_recyclerview);
        layoutManager = new LinearLayoutManager(getActivity());
        view_cheque_recyclerview.setLayoutManager(layoutManager);
        view_cheque_recyclerview.setNestedScrollingEnabled(false);
        view_cheque_recyclerview.setHasFixedSize(true);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo == null)
            showSettingsAlert();
        else
        new ViewChequeApi(getActivity()).execute("http://13.126.131.147/view-cheque");

        return view;
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else
                    new ViewChequeApi(getActivity()).execute("http://13.126.131.147/view-cheque");
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
