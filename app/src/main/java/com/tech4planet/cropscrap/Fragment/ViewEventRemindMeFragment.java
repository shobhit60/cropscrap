package com.tech4planet.cropscrap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.tech4planet.cropscrap.Background.ViewChequeApi;
import com.tech4planet.cropscrap.Background.ViewReminderApi;
import com.tech4planet.cropscrap.R;


public class ViewEventRemindMeFragment extends Fragment {

    public static RecyclerView view_event_recyclerview;
    public static RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    public ViewEventRemindMeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_event_remindme_fragment, container, false);
        view_event_recyclerview = (RecyclerView) view.findViewById(R.id.view_event_recyclerview);
        layoutManager = new LinearLayoutManager(getActivity());
        view_event_recyclerview.setLayoutManager(layoutManager);
        view_event_recyclerview.setNestedScrollingEnabled(false);
        view_event_recyclerview.setHasFixedSize(true);

        //code for checking network connection
        connMgr=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo == null)
            showSettingsAlert();
        else
        new ViewReminderApi(getActivity()).execute("http://13.126.131.147/view-event");
        return view;
    }

    public void showSettingsAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("We can not detect any internet connectivity.Please check your internet connection and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                networkInfo = connMgr.getActiveNetworkInfo();
                if(networkInfo == null)
                    showSettingsAlert();
                else
                    new ViewReminderApi(getActivity()).execute("http://13.126.131.147/view-event");
            }
        });


        alertDialog.setCancelable(false);
        // Showing Alert Message
        alertDialog.show();
    }
}
