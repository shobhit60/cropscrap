package com.tech4planet.cropscrap.OtherFiles;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;

import com.tech4planet.cropscrap.R;


/**
 * Created by tech4planet on 11/1/17.
 */

public class DatePickerFragment extends DialogFragment {


    LayoutInflater inflater;
    Button button_cancel, button_ok;
    DatePicker datePicker;
    setDateListener dateListener;

    public static DatePickerFragment getInstancce(setDateListener sd_listener) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.dateListener = sd_listener;
        return datePickerFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        inflater = getActivity().getLayoutInflater();
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.datepickerfragmentlayout, null);
        datePicker = (DatePicker) v.findViewById(R.id.date_picker);
        button_cancel = (Button) v.findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        button_ok = (Button) v.findViewById(R.id.button_ok);
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.putExtra("day", datePicker.getDayOfMonth());
                intent.putExtra("month", datePicker.getMonth());
                intent.putExtra("year", datePicker.getYear());
                dateListener.onDateSelected(intent);
//                DutchIt_SharedBill_Fragment sharedBill = (DutchIt_SharedBill_Fragment) getActivity();
//                sharedBill.setDate(intent);
                getDialog().dismiss();
            }
        });
        dialog.setContentView(v);
        return dialog;
    }

    // Container Activity must implement this interface
    public interface setDateListener {
        void onDateSelected(Intent intent);
    }
}
