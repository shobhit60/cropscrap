package com.tech4planet.cropscrap.OtherFiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import com.tech4planet.cropscrap.OtherFiles.SmsListener;

/**
 * Created by u on 5/30/2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener mListener;

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            for (SmsMessage message : smsMessages) {
                // Do whatever you want to do with SMS.
                String sender = message.getDisplayOriginatingAddress();
                sender = sender.substring(3, sender.length());
                if (sender.equals("CScrap")) {
                    String messageBody = message.getMessageBody();
                    mListener.messageReceived(messageBody);
                }
                Log.d("sender", sender);


            }
        }
//        Bundle data  = intent.getExtras();
//        String format = data.getString("format");
//        Object[] pdus = (Object[]) data.get("pdus");
//
//        for(int i=0;i<pdus.length;i++){
//            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i],format);
//
//            String sender = smsMessage.getDisplayOriginatingAddress();
//            //You must check here if the sender is your provider and not another one with same text.
//
//            String messageBody = smsMessage.getMessageBody();
//
//            //Pass on the text to our listener.
//            mListener.messageReceived(messageBody);
//        }

    }
}