package com.tech4planet.cropscrap.Pojo;

/**
 * Created by tech4planet on 22/11/17.
 */

public class ContactModel {
    private boolean isSelected;
    private String name, number;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
